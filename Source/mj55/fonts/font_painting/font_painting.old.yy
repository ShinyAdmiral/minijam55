{
    "id": "8faf8c4b-3d68-40fc-a143-457594bc01d6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_painting",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a0de141d-b4a0-426a-9381-9853e570fdbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "8bf2af86-f797-4ad1-9bac-9f9fd08273bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 238,
                "y": 23
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e9b79616-f9b2-4073-b9bf-fe7685a8ba8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 232,
                "y": 23
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e52e3998-8101-4165-a94f-6d61eecd8243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 222,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "877e8b2d-a938-43ec-8de8-2c4929575125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 214,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "39c5d5e3-6d6a-4875-842f-785ab0e95c40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 202,
                "y": 23
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "73f21545-050d-4a62-98ca-846366c64ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b164bd5c-ae6a-4d82-abaa-15e9ed7e88a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 191,
                "y": 23
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4fa930a1-72a9-44c5-8921-070c1fee3378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 186,
                "y": 23
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ee359e36-c339-4ee8-8614-53e688708fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 180,
                "y": 23
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b70ebe0b-b190-4c40-8209-6ddb24ba337b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 242,
                "y": 23
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b3b4f5af-481c-4ba9-bbc4-8a7ea102e6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 172,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0bea6695-d863-4c9a-ad25-26c1e1b0760d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 159,
                "y": 23
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "84437cd5-698f-43bf-bb47-92cd11fa8c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 154,
                "y": 23
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f5752e4b-5b66-4f28-acaa-38fb380c66c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 151,
                "y": 23
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c52f0c3b-ab4b-4277-bbed-5fd250bb391c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 142,
                "y": 23
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ed00669a-837d-4d7d-b2d0-193dc6c704a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "7c1bd0fd-46c2-432b-a89a-fb67eb915961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 129,
                "y": 23
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1f72843e-9c19-4db7-af12-a656b5ce14f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 122,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "43ca083f-b99e-4d3e-bf7d-e4eea58d4c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 114,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "69d86675-a89c-4374-a119-2c9696eeecc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 105,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "adad36c9-971a-41ae-96c4-9c09d7366ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 164,
                "y": 23
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "18777bef-fc27-4287-8dca-55d9ad2d8bad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9bd3f214-5b53-4378-8a64-60a64cfd36ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 10,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "43c0d105-b488-489f-8a0b-51d20106e661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 19,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "84093ef8-4f0d-4bdb-bd29-9230b5165323",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 169,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7c5efe3a-3004-4a93-81d2-8272ea39ac1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 166,
                "y": 44
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "02ca167e-9c48-41f5-8eb8-51715d870637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 162,
                "y": 44
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8ff27bb1-4b16-4b93-897a-4ff946129a1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e0e0bc6b-cc6a-4fab-9bed-686bba6fa843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 148,
                "y": 44
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "77617f3e-a1a2-4fe0-b824-2f382eb6afa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 142,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "50566f5f-60b3-4010-adcf-6a89c4287caa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 135,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d82a7dd2-c78e-4785-84f3-99d4ac3caec2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 125,
                "y": 44
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4564de49-514e-4633-b9ab-bf36b65650f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 116,
                "y": 44
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "04d8c5b3-a1ca-4a5d-9839-b878d0d61251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 108,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "8341bf7e-2114-490d-bee5-e0ab02fd84d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 100,
                "y": 44
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "95e14e69-963e-450a-9e36-037ae809140f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a85d1374-939e-4d2f-9c18-a47d81c07fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 85,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "799c4c32-f01f-4cce-904e-dcaf89c5cb0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 78,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "be34d41f-2acd-4a1a-9b2b-e3c4b00c9617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 70,
                "y": 44
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2380be50-9489-4166-a3d5-c15dcb517432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7c1aa722-96b2-4760-b9d7-afc0712ab1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e05ef7c9-e18d-4668-9d72-1e16c252d902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 51,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "583d943c-c6f1-4999-b624-8692ae487928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 43,
                "y": 44
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "405c65e8-ce5e-43eb-b228-7fe4c6fbf487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 36,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e7ce0f18-d572-40fa-9c7c-ab67184f3dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 27,
                "y": 44
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5bfa565a-599d-446e-b142-4463507b2769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 23
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bb61a751-0946-4004-a591-3ef4bb546d0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 89,
                "y": 23
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a1ff8b4c-9635-4193-bc33-940efe7a41e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 81,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e8b5391b-a537-4526-add9-29c55b3ccf54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f626842a-afaa-4bbc-b8cd-dc5757b83adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d229c90f-e8b2-4096-a3c1-9a69cd4e5b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d03eaaf7-dea1-4388-897b-41e7ef6ce0ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cb55369f-4fea-4b62-a0f3-b0399e99523f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "65b06d71-a8e5-4cc7-98e2-d6726f3c6c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4dd79afa-c694-408f-8949-5d6be878acac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 108,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ed0b7f55-6ec0-4824-86a3-1d9b86b25d4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d3398008-ba60-4c2e-900d-777c4fed9d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b2b6ebf3-15c0-4bcd-8ff3-483b51861c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "569a1c7d-0a96-49c0-a16a-54aea042a04b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fa6ef252-5551-4bcf-b51c-f30902de797f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e4a2d488-b690-46c0-801c-365566727c4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "25599ec7-c803-4744-b04b-e140d12a20f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "bc775d50-64b6-48cd-8667-51c61cef173f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "207c9ee4-3618-4b4c-b9a5-ae50495faa33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2f6a1882-94da-440d-b013-eca16e5c402c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "c275c71c-ee78-4ad8-a0b5-b34ffd0c8e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a2ce9213-692c-47ad-b48a-e242c260da45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d4cdf4d9-66f3-48a9-8595-fe886d66d999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "09895f10-e585-4bee-aec5-9567949e69bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ed435e0f-160a-417c-ba70-f22073c57ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "76a6d2bf-49fd-4e47-8199-b32d2dccce88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b3e4a74d-327b-4071-9aba-109e02705016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "c32bce8f-7a63-4804-8dbe-3a62c402af56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5a7d8c43-4d6d-4c42-9e4c-9f9d87d65076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 70,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "44456adf-5ff9-411d-bd74-3decd130e65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 63,
                "y": 23
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "f94055e8-6002-4842-9856-e975e63b1339",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 60,
                "y": 23
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9fccc7fb-a31c-437a-ad3c-5e83f0acf6e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 50,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "be0391a8-7113-498d-9403-d518a70070e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 43,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ab7e1d42-4353-4b3f-a79b-6fe81d6b89a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 36,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cbda301d-c384-4cee-8666-f26cbf012b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 29,
                "y": 23
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "cf02d5fe-a6ca-4a32-9193-e04146fdad7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 22,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "834ab4e9-c59d-4966-8354-63b2ad24d7fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 15,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "6d4d70eb-3353-4b1f-a18e-3c4bad177743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 74,
                "y": 23
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8bcbe666-339a-41f4-9025-911c8b75cb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 9,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6579b1a5-4b74-49d6-ae20-196f9d143a1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5dcd728d-98cf-4463-ae8b-375d715faa92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e7d1ff6a-9255-41ae-9532-033c5e90bad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8a0130cf-6f9a-4b1e-89bc-377865d36161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "748b818d-4742-42cd-8d45-868b4fe30fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e1924f8f-839b-48ba-9273-10500e7dd259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "852c4586-717a-4921-8523-ccfb9071b616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "832d6468-dd77-412c-b4b2-e22f4fcd9a0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7fd7aa49-bfb0-4092-b305-875448c2bcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "cc9c5a33-5f1b-4362-b403-85e774b8af4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 177,
                "y": 44
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ff9aa1f3-c9ca-4f59-9bc3-d117e5381646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 185,
                "y": 44
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}