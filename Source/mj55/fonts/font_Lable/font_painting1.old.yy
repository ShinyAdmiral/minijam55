{
    "id": "8faf8c4b-3d68-40fc-a143-457594bc01d6",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_painting",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Agency FB",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f7d4e3da-6981-4cf1-b247-c0b2e5822fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2b00a30c-71e8-4404-ab0c-eea2188d5779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 64,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f0f5a9cc-81bf-42ea-bac2-d6e3f31378b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "941c72e9-25aa-4acd-af02-e06757eca423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 45,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6deca864-6916-4685-b8ea-d7070616a9cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 36,
                "y": 50
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e23d16cc-2c56-4512-9c06-52d8c20c1752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 22,
                "y": 50
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b46b43e9-c99c-40db-a489-8c5de90ff152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "fba70e8e-2b98-436e-a8e4-79a60a7b1f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 8,
                "y": 50
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1e951709-d8ca-48b5-a17b-95a7101e8325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "48a7bba5-5595-4472-b507-212d301cc3c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 244,
                "y": 26
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f480d0ec-0010-44cb-b1b6-57eed3e7d593",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 68,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bbb3136a-10d1-4c5e-9877-fb2bdd7b32ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 235,
                "y": 26
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4ae1d11d-942e-45b7-a0fc-80bb68e5c37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 222,
                "y": 26
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a445f28a-1675-4b80-9dcd-51f5e82e2314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 216,
                "y": 26
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7861a54a-8a0d-47d8-b8d3-56aa08680610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 212,
                "y": 26
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "612062a4-a3e0-4269-a137-442013510670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 202,
                "y": 26
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d4a6f60e-826c-44f4-8ebb-655cb2b92752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 193,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f1442637-06d9-42ca-b57b-1d52dc5f739c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 188,
                "y": 26
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "277bb7dd-43de-426a-a807-96a903f11355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 179,
                "y": 26
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6dd504e3-f009-4301-8a11-84f962d7858d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ae835e07-6625-4593-a021-28864b008e64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 160,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3210f844-2504-4564-a2a0-58e0a6f891c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "dcbf9553-056f-40b1-a37e-ea3eb25887e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6e9b76ac-ba62-4056-93a6-91cc933119f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 86,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6f9a7d9d-d8e5-45ac-b090-90e327806f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 95,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0179df76-1989-4f80-a077-41c66a43804a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 18,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "92d1d81c-678d-4623-88f2-7d5a495111f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 14,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "553f9aad-bbbc-4756-a052-c464d9dca3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 9,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9d27ffe5-7792-455f-b1be-731e003af8c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e8143820-14d8-417f-b57d-a915804337f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 241,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d654cd2a-19fc-4a40-aa25-5183bb783c72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 234,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "77a80cf4-85cd-4ce7-846d-3b3bef16a46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 226,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4e2614da-b4af-49d1-a001-6989ed87045f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 215,
                "y": 50
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9c4a7bd9-6983-4c8f-a6e6-144d5833b0e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 205,
                "y": 50
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7c7f5a8f-e545-4c71-8d8d-a1386d7cbca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 196,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "afa4ad12-d5b2-4cd0-ba48-85685a90e5f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 187,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dba6da5e-c896-4683-b092-5fc9c2add57c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 178,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "cbcfb8bf-b994-4a4b-90ef-addcb761ba48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 170,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a7513698-fbdb-4f7b-84da-57be097b3eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 162,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bf6487ab-f90c-4de3-aff4-8d7c29e49791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 153,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c2379780-32d0-41a4-9318-5643bf3e37af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 144,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "db4f305b-cd6f-413c-ab2c-7ec4ff7ccdf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 140,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b5132f7c-1d3e-4cb6-8c29-5e756113a661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 132,
                "y": 50
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f0677280-8f03-479f-8514-9d46986e92fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 123,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "604cc1a1-ee0a-4422-b053-f786b0785296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 115,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "13859479-aaec-4c39-8a63-3d718540e55f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 104,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3abf60d9-80af-4f26-af97-d9a93d34fb9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 151,
                "y": 26
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5ef6fd00-165a-48e4-8f02-768b082113e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e36f353e-eb01-4eb2-91d2-e39a669d0127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 133,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ecfaaf80-cf23-4946-8c9c-8b17576a9617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "17f72490-f495-4d06-bf96-7d57f0f4358b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ffc9d173-64d7-4c5b-9328-85ae61de69f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8dc3ee50-6a7f-4dd9-b277-f4aa83e9da95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bf48a4b5-5e23-4424-9eab-e813ae404fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "98ce2417-807f-42c3-bcf8-b78aa14c9d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d84073ec-19c7-4f5f-a015-8167360ea2f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "fef3f26a-67e5-40ed-92cd-34a3e223520f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "22bbeb93-c2d9-43b6-95f2-4cd180f9a5cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2b826fc0-34f6-4879-b1cc-802966b26993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a17dc446-0945-4be4-b298-93769687aefb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f056fdb8-0cdb-4cd5-b964-918c863338c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "fd40d141-653a-403a-b54a-f502e05a59b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4482cfd7-2571-4c3c-aef5-5d91b07d1c61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3c1b1375-8b95-4c5b-bfbf-efbfc91fe123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ad0cddc6-d9cc-42e4-b34c-4a4d550ea008",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b4b745fb-76cd-4263-9a6c-a2edb9beebfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5495390a-8e89-4e52-abbd-d8c2a0f4116a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bcffb478-4cdf-4b5d-a3af-059bbb54c5a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "00bb4be7-84e3-4ccc-97c1-cb40142578de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8f8d1a9f-7efe-4b8e-8def-8116415069ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5d306166-a23a-473c-b149-a001da377409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "70c6d0ff-f578-48f7-af55-7c56971aff15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "dffb582d-b39e-4bc4-9492-4ce47fb30d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 40,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "56479058-8a50-4cf8-9cc4-729c9c1b7e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d8c916d0-2bde-40c2-b77f-c8438c76141a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 119,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "690cf2b6-2701-4ed9-ae20-77e4ba1e2097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 111,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5750cf59-58a6-438e-95fe-9a4446ac7fde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 107,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4eddce95-bbac-4565-ae33-12ca143ab7a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 95,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0a3d1fed-bd9f-42c3-b98a-dce9cd581235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 87,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "11c738f3-e6f5-442c-b3ab-36f3320b791d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 79,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e8ae5323-e1a0-4a43-8371-5ccc306ed365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 71,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e823880e-6563-4fce-b571-155b3337fba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 63,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "09774559-101d-4818-920b-029d8441fb1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 55,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ed7fe317-0d30-4a03-a767-f08ebda59e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 125,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6526bb6f-1464-4b86-8840-5c4d7046f002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 48,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d67447c0-7a25-4c2b-832a-18b12ef5931d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5bda3fbf-ec66-4ace-af2d-44b7a00f4fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8b0ee503-cf25-4d85-83de-f5750125af56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 11,
                "y": 26
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b490e104-042a-436c-803c-bcc5c0cba7bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ef214c00-4a99-4fbe-8a3c-f071f24f0eb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b810f793-735a-4195-8f4a-5f01a8670aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6863c8c0-b80e-4112-8df0-204e8842b918",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "61d46494-2804-4596-a2c1-6c438d456b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "afa1d5be-dcb9-4f1b-b1c2-93acdf4d8e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1e0c9bbd-dbf2-45f4-9d4e-2a4f67e5e7e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 27,
                "y": 74
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1ede125d-6c8e-49cc-b19b-fb5109e365f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 22,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 36,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}