{
    "id": "903d356d-0333-4f6c-91d6-722fe998af95",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_Lable",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Eras Demi ITC",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "62011714-47d6-4662-825c-8684d497f92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d42cefe9-6243-4cbc-ae33-00dce25a4773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 202,
                "y": 95
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "764e645a-d935-4574-87b8-70d5f5b3961c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 194,
                "y": 95
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "94658e5c-53cf-4cf6-988d-4828f6e26f96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 95
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9da4705f-0aa3-45ae-8ef0-3296247517a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 165,
                "y": 95
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2176d097-94f1-4524-b605-2f3902ac44b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 143,
                "y": 95
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "18799eb0-29f9-4b62-8fa9-fa252437cc64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 124,
                "y": 95
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "267f4067-0e8e-45f3-b040-2ed2aaf2b7d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 2,
                "shift": 5,
                "w": 2,
                "x": 120,
                "y": 95
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c6e6e3eb-1428-485f-8448-07f95dc56adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 113,
                "y": 95
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3823b810-aba4-4e4c-8f99-492bd90ba86e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 106,
                "y": 95
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8362d018-5799-4974-8fac-f61bb7b300b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 208,
                "y": 95
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "11b8e5ba-ab2f-439c-83f9-d946d4227bb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 91,
                "y": 95
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ad214469-608f-43f2-9360-c8b2082aed28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 70,
                "y": 95
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9a349d0d-73f3-4c8d-857e-7888676e2b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 62,
                "y": 95
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c6cb84e0-76e0-4b3e-85e4-cba610a9e785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 56,
                "y": 95
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a7348cde-7d5b-4d5e-ae8f-f2415397a517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": -3,
                "shift": 11,
                "w": 14,
                "x": 40,
                "y": 95
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3b9ea1c6-31c4-4f54-b564-b80dd6e543fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 25,
                "y": 95
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "48ff117e-4d9a-4348-bb82-d194c1433718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": 5,
                "shift": 15,
                "w": 6,
                "x": 17,
                "y": 95
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "efd2e945-be6f-43d5-8de3-aed94f8540a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "956c8e6b-0259-42ae-bd6b-5b38100ca494",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 239,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "b4426cd4-2801-42d5-a3c4-d54b07acff1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 224,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b7057892-0694-4f7e-b2a9-f2a1d4278333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 77,
                "y": 95
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "bd381332-2490-44ef-b01d-c8363f0017e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 219,
                "y": 95
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8559a58b-6179-4e99-8cc4-d897dc42c0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 234,
                "y": 95
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "867721ab-0395-4972-86ce-8106ae0e94db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a8e41051-5220-4c50-bc50-705b574d6357",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 69,
                "y": 157
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "32b54af8-a846-4229-a948-8239ca9bba06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 63,
                "y": 157
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d1d9b410-debb-45df-b33e-2b1838bb2e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 56,
                "y": 157
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c11e7b26-c44a-4625-bc09-e850a1d317d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 43,
                "y": 157
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f74b78f0-e090-48fe-aafa-bc9105bb6a5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 28,
                "y": 157
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ebe25945-17ac-4df5-b718-1b4b91d00d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 15,
                "y": 157
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "081baff7-bf82-4a8f-82c7-5aebd8efb992",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 157
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3e77a6e1-de25-419b-9b8c-3970534c0a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 232,
                "y": 126
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4e12c408-5bd9-45fa-8049-ffe02693cf66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": -1,
                "shift": 19,
                "w": 20,
                "x": 210,
                "y": 126
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "65963b95-ee2f-4b71-bf69-dc3fed2bc44a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 126
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "88723331-5eb4-4d5e-baef-e9967cfe2669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 126
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "200ba277-f57e-483b-80cb-d5cded2c2e3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 159,
                "y": 126
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7251cf8e-0063-46fb-a5b3-cb16dfd8a95a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 144,
                "y": 126
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8d9a482e-14cd-46c0-99e2-f56167946d8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 129,
                "y": 126
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "886a5298-d20c-4579-b77a-f1383098a5f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 110,
                "y": 126
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1efa9f93-5949-4e11-92a4-e08fa63f987f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 91,
                "y": 126
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0b3a9349-c6b6-4b7a-8467-be723a778514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 84,
                "y": 126
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "90675236-dc77-473d-a386-a6e836f2130d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": -1,
                "shift": 12,
                "w": 11,
                "x": 71,
                "y": 126
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "38e84870-dd0d-4b18-b74d-6dda884079f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 53,
                "y": 126
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d8b16ef4-022b-414e-bc0e-0ea781a380dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 39,
                "y": 126
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9a554b6d-9f5d-4c9f-a9db-705f0d285d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 17,
                "y": 126
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8b9ffb43-c501-4d36-87c1-17ef905c3bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 205,
                "y": 64
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "600d6d7b-e6d1-40cc-90e8-9461d25aa5fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 184,
                "y": 64
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6c0ebf30-56d5-41b9-b367-a417b2e5bda0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 169,
                "y": 64
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d25aa558-7a7b-46f8-b46d-956e7a27b331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 74,
                "y": 33
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fedea28b-07c5-4b95-9986-f1bf6f4a339e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 33
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "79631624-bc85-475d-ac32-392ff2ae4763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 37,
                "y": 33
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8a88c1b5-bebb-4ffb-a2e2-9171051409cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 20,
                "y": 33
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e341b755-8e13-4575-9f64-3022ecdd29ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5c38d230-5f42-4440-9377-4e525430dcee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "16aff20f-8d7b-4a15-93c4-d130c28e187d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d00592a8-55d8-4d03-a287-386b976d3ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": -1,
                "shift": 16,
                "w": 17,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9ab243b6-1d80-4aa7-8db4-dcac997074a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4929135c-7690-476c-9ca4-b62188d49131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "63e603fb-d38a-4b33-b136-cbe645b960dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 66,
                "y": 33
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e4303dbf-dd72-4e97-9f4d-ba29bedfe468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "153b8fad-f1e9-49af-9e0e-ab9aedd2a8fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "63547e54-bd79-40e7-ad54-905be3c7fc7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0eaefcee-8155-4f89-b642-d30f00684b70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c87db5bb-87ed-4e03-9cee-eb49f784ddc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 3,
                "shift": 13,
                "w": 5,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5288ed28-28b6-432f-a716-cb291aedead1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ed119a01-78cf-47f9-b0e3-bc05d564281a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "592b7bd9-72eb-41de-92ed-dfaefdde3d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5cdeaed4-eb7c-45e4-893f-78f135210d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "50ac7cf3-ba4f-4c9a-ae82-1acdc1d07257",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d0099a3d-c1a1-43d8-8baa-9b70921aef5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "94736954-0ba5-4a14-9feb-4c3656614bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 95,
                "y": 33
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "e5f041cc-937b-40c9-949a-c2b882f524fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "beba8b99-0da1-4689-8b3b-86948b4b7844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 111,
                "y": 33
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "dfda5c1a-b13e-4a1f-aa3c-0e9bb70a82bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": -4,
                "shift": 7,
                "w": 10,
                "x": 146,
                "y": 64
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "fb92e04b-7eb5-489a-b1a3-94fed3568c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 131,
                "y": 64
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "21e86c1d-0ef6-4ebc-96a3-d341f634500a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 124,
                "y": 64
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "727f3b08-c436-4ce6-90f1-42bf1b61cd68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 102,
                "y": 64
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dae3dc86-869c-49ec-8a67-7aba3fb3b492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 86,
                "y": 64
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d834b920-3f74-4cd0-a0c7-bcab347c65e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 70,
                "y": 64
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d00f0774-29ec-4c44-a480-6d9fb31c1d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 54,
                "y": 64
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b46220e6-1655-458d-8c91-bdd861d65dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 39,
                "y": 64
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "1fac8c02-6936-4282-994e-5d7933a40bc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 28,
                "y": 64
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "14510530-2ebc-4517-b5e1-7e71c82babbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 158,
                "y": 64
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "afe9e96b-3ac8-4d32-81c9-1212099e6abe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 64
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "feb9b51f-d200-41a2-8d14-403eaad94110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 226,
                "y": 33
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "44f12549-b3d8-4b2c-a965-b5dc2b98c5df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 210,
                "y": 33
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "95f35e7b-5ac8-40b9-b7a4-cec7f866e046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 186,
                "y": 33
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f5bce6c1-f2d0-467c-bec7-a4a626f75d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 170,
                "y": 33
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "dc6d7fbf-4cdc-47a7-975d-b48b73e4abed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 153,
                "y": 33
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2301e130-822f-4f1c-a90d-4a4ae07f82a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 141,
                "y": 33
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3eb4192b-dbbb-44a0-9915-ba7648db2aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 132,
                "y": 33
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c32ad199-291d-47fe-a1ac-0639990e6218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 127,
                "y": 33
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b9e99521-31bf-4353-9242-6abcb6e78a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 118,
                "y": 33
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c3d004e6-30f4-4e31-b129-4c4e1af25e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 2,
                "shift": 17,
                "w": 12,
                "x": 84,
                "y": 157
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "3e1b001f-fa15-431a-97ce-a7b42bb3bfc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 29,
                "offset": 5,
                "shift": 24,
                "w": 14,
                "x": 98,
                "y": 157
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 19,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}