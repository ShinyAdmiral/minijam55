{
    "id": "903d356d-0333-4f6c-91d6-722fe998af95",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_Lable",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Eras Demi ITC",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "62e8f52f-5c7f-42a6-9f3e-d30af9693a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ccff2abf-76d1-4f1b-bc91-8b886013e1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 248,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "256f1929-e640-4378-bfc6-3db823b638fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 241,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "99691d04-9d41-4e05-a56f-c31e1e070fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 229,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "94d13db4-78da-4b0d-8a2e-637b49bff78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 219,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a698d5b6-4553-41e9-8603-93b85622ef88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 201,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "40088b58-15d9-44b8-8c6c-707b1b396af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 187,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0e51158d-603b-4f46-b2d9-c4cf61b3ab09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 183,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a56cf5dd-b518-47ea-810c-393925e3eb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 177,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "41f9ca4a-4e4f-48c3-8b7f-2400640a950a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 171,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "bb9909b5-a76b-4c4f-a3fc-2df19a5117b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2427b61d-69e3-4aab-8821-d31554ca6634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 159,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "31710e18-379f-4daa-ab32-34ca85b21bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 141,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "02b98d85-57ea-4530-b4fa-bdf1f1291b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 134,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0644f4a4-aff2-487e-bb6f-75106a67c214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 129,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0dadf5e8-e07f-46dd-8e3d-cce28be467c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 117,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e2694f16-b0f4-4e59-96c1-760ee9cb27da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 105,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "12f9a7ca-ac3b-4216-bc60-d2df02a86c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 4,
                "shift": 12,
                "w": 4,
                "x": 99,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "10a31aa7-4dd3-41f9-9914-29617f9c358e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 87,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b67994b1-1a0d-47d1-8978-7ac3dfd04385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 76,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "779f2a37-13c3-48ff-b060-9a09638f7f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 64,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b9cd8378-edd8-41d8-8275-db498e7707b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 147,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4f552068-92ce-4a46-92ec-854cd07b64e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 10,
                "y": 71
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0ded35d5-d400-442c-8a72-676a4581548e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 9,
                "x": 22,
                "y": 71
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2086564b-29ff-46ee-9314-880be9a776e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 33,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "68413d89-66f6-458b-9812-d68579cea72e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 35,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "cb8b15a6-8e64-4382-b88d-699c7def55ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 30,
                "y": 94
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6f00fe96-48a0-49fb-8044-5574fc46d994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 24,
                "y": 94
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9995adbe-84ed-445d-a1e5-9c5cc2240861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 94
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ed8cb569-d778-4a46-aa1c-60bdeebdf3ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e60984d6-51d9-40a0-9361-2cfb25da60b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 239,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "83cfbb09-be01-4a38-acc0-812e895a717e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 229,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "03a1af08-49dc-4b4a-ab18-0de4f034462b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 213,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "28495347-5ded-46ab-b7f0-f592abe0f924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 196,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e5a49022-c087-4853-bb2a-8bee014cb3ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 183,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d252a1dc-f5ef-42f2-9e9a-21e0b7bf1b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 170,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "43091999-2231-4339-ae40-e022517fd16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 155,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "fbbe711a-f057-4963-aace-e975f3d38714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 143,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e2b83eec-1ddf-483a-93f0-ebcc0a567e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 132,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "5ebbf88f-4829-4bea-a040-5687cd5f735f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 118,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8f04030f-3c05-4e96-8a2e-2ac2dc797878",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 103,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d959f8b8-1feb-48d8-85b2-da3aa64581d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 97,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "41c6f6b8-23e2-47d5-8c3b-5af8097b7b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 87,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "90a3e59e-4a29-46e4-a8b7-cc0babf3f1bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 73,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "289f56c2-4c5c-462f-a172-e25584bfff6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e9348417-2fc9-4f89-959e-7c5fd7c8a747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 45,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b1c3d06a-3b26-499c-b077-f4172c3740b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 49,
                "y": 48
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6022379c-60d8-4b15-9732-d760e39002cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 33,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "3e5f6d24-0677-4214-932d-e3bdfbfdcc89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 21,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "af748ecb-14e5-49dc-9558-7f8bcd594ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 9,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ec658117-150b-4150-bc49-ba206bbe8c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fd5edccb-b922-499c-9236-6bb166d10541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dc767cd1-d891-43ab-8312-800470db9094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bf1b8420-93ed-4e95-aa51-c166508d50ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b0c358a3-e980-4df7-9332-d2e91e719d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a06ab766-6e87-4d45-9adb-e71f55c11f9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ebcd0d58-2880-4faf-9afa-b06ec80236ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "21c91641-acd7-4af3-a1d9-e3930a60af5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4e17c637-24ab-4da1-95b5-5df5e80d2b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "41d0ff01-b781-4b7b-bab5-2b505543ab0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f40b320c-5751-4da8-a48f-e88be64426c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0a16904f-6564-43eb-aa3d-d97655730a9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "72eaac74-d6ff-4483-8014-0c892d20da47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0362110b-71c8-47c9-a69a-31f54ce10ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8027a799-04fe-46cd-abc6-e12ddd5ee3ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4458a696-d873-4996-bd4c-d1afd516d59b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "749fcff6-0bbb-44f5-a71b-7476dc2e253f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6522fdfa-30db-44c1-9570-7a8e1056aef2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ffb0b758-131f-4827-8b87-add766cfc36b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f5d940d8-dcf3-4e5d-a12a-70451a1fe99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "df24ec93-9fdb-4ddd-8ef6-67c4f8e9f5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fff4ffa5-51fa-4890-9824-4998fa88ede0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 25,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d3c0bbdc-491d-4a8f-aca8-14feaa18ba80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ced59df2-2f18-4fb0-a529-ea718809beec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 37,
                "y": 25
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "81790927-b266-4ae7-8f0d-922a16ec3cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -3,
                "shift": 5,
                "w": 7,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8179025e-005c-4bb9-83ae-a05855aed090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bbf11b88-79d7-465d-86a6-fc6cf9399c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 232,
                "y": 25
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b99a8cff-03a2-40ba-a35a-40529fea3747",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 215,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f581c972-1f0c-494c-9c70-c0eb4be2ba38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 203,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fa93c63a-1a50-400b-a5cc-d33532ed7d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 191,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9d624caf-4372-4413-b9bc-a10d74df407d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 179,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "93d504cb-5e7c-4da3-934e-460ad1be9347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 167,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "14978399-cad2-4a59-ba81-c1390970d745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 159,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d4986715-72a5-4621-94bb-a116444c7778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "297fcc0a-b1a8-4e01-8688-db0771dbd261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 151,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e5648450-eee0-4f5b-af6a-1e9be404f2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 127,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "511a10ed-09bf-4566-88cf-6492cea50a63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 114,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5b1cbea7-2365-4a72-a0b9-09ad6baba6f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 95,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5f04c1e5-6f44-4cb0-aedb-c864ab0bf33a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 83,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2cd7013f-f158-49ac-ad34-ac0719f9c5bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 71,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f53b5b19-a28f-45d4-a9a4-62664ac8c90a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 61,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "29de26a0-ca9b-483e-bd0f-dc494bb4a087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "53fb1e77-64b6-4b49-8744-946a00a63668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 49,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "064f3838-9171-4d75-bb36-dc116865ab70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 42,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1c9c5208-8937-478d-87bc-6b5bcd5a7626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 47,
                "y": 94
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "06f47b38-ef82-40a6-b0fb-18ee23ff2d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 4,
                "shift": 18,
                "w": 11,
                "x": 58,
                "y": 94
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}