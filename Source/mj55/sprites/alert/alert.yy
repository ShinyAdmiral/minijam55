{
    "id": "72dbe503-e617-4e43-b014-0bb6bf4aebd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "alert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 13,
    "bbox_right": 18,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "950aaf32-651a-4f5e-a739-fde5a22e2827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dbe503-e617-4e43-b014-0bb6bf4aebd1",
            "compositeImage": {
                "id": "b9090694-bbd5-43fb-a96d-75bedeab5082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "950aaf32-651a-4f5e-a739-fde5a22e2827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d852137d-ac1d-4cae-bcaf-086329c33d21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "950aaf32-651a-4f5e-a739-fde5a22e2827",
                    "LayerId": "71c6a1b5-5975-4c7b-8c5d-2ce6821926eb"
                }
            ]
        },
        {
            "id": "653ed4c7-1542-4450-87a0-60a5c5d159f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72dbe503-e617-4e43-b014-0bb6bf4aebd1",
            "compositeImage": {
                "id": "531af58e-51b6-4415-b491-58ea3fdff257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "653ed4c7-1542-4450-87a0-60a5c5d159f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96999df9-78eb-46fa-8831-f18b71f677d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "653ed4c7-1542-4450-87a0-60a5c5d159f7",
                    "LayerId": "71c6a1b5-5975-4c7b-8c5d-2ce6821926eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "71c6a1b5-5975-4c7b-8c5d-2ce6821926eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72dbe503-e617-4e43-b014-0bb6bf4aebd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}