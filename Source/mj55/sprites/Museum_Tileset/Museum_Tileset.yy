{
    "id": "4e3facc0-1d5d-45b3-a786-8492cb80a70c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Museum_Tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 32,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "718cf88a-01d1-46dc-a8dd-ade2f385f469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e3facc0-1d5d-45b3-a786-8492cb80a70c",
            "compositeImage": {
                "id": "f1233a2f-722b-4bc5-b4c9-95e35952ba9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "718cf88a-01d1-46dc-a8dd-ade2f385f469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3836a4b0-8d2c-4add-be15-56df9aa25001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "718cf88a-01d1-46dc-a8dd-ade2f385f469",
                    "LayerId": "bf36eeef-c6a8-4143-82e0-c3f0a523df7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "bf36eeef-c6a8-4143-82e0-c3f0a523df7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e3facc0-1d5d-45b3-a786-8492cb80a70c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}