{
    "id": "0a9ab106-41b2-48ea-aff7-d7b6db8ccd3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 3,
    "bbox_right": 9,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57029b77-7091-4f04-ad05-ae10445abc17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a9ab106-41b2-48ea-aff7-d7b6db8ccd3c",
            "compositeImage": {
                "id": "e72a81f4-643c-4321-8525-67758e7d14fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57029b77-7091-4f04-ad05-ae10445abc17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddf4359e-4d7e-4750-9675-4629c4603c31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57029b77-7091-4f04-ad05-ae10445abc17",
                    "LayerId": "da81220a-8fa8-424b-a3fc-31bf22b4dc61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "da81220a-8fa8-424b-a3fc-31bf22b4dc61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a9ab106-41b2-48ea-aff7-d7b6db8ccd3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}