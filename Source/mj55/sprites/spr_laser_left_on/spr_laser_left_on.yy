{
    "id": "e3d9a3ef-3996-4bd6-8061-fc0c4a02e62d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_left_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8343ba0-ddfc-457f-92b0-1ae23fba7be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3d9a3ef-3996-4bd6-8061-fc0c4a02e62d",
            "compositeImage": {
                "id": "639b3c59-42ed-4095-89dc-ccff5630e877",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8343ba0-ddfc-457f-92b0-1ae23fba7be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "131f2769-e14d-421f-8373-1531f1c26852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8343ba0-ddfc-457f-92b0-1ae23fba7be9",
                    "LayerId": "d432f892-4525-49cf-9def-03afc0febc62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d432f892-4525-49cf-9def-03afc0febc62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3d9a3ef-3996-4bd6-8061-fc0c4a02e62d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}