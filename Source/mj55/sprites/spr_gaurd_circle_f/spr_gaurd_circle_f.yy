{
    "id": "5659f7e7-1a90-40f6-a90e-06ec46b6bb44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_circle_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b4afa43-25fe-4e85-a3af-c82d6ee0fb1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5659f7e7-1a90-40f6-a90e-06ec46b6bb44",
            "compositeImage": {
                "id": "d2d61049-74af-4eda-b004-341751c0fcac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4afa43-25fe-4e85-a3af-c82d6ee0fb1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92b55d62-da83-4a9c-9d1b-671b06611b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4afa43-25fe-4e85-a3af-c82d6ee0fb1a",
                    "LayerId": "f9c47a3a-54eb-4c20-bcd3-7f1dc809184d"
                }
            ]
        },
        {
            "id": "3f439eef-a6e9-4fd7-984c-dd90ebe49005",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5659f7e7-1a90-40f6-a90e-06ec46b6bb44",
            "compositeImage": {
                "id": "6c2f49fe-5628-4462-8639-53a1fb7a5338",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f439eef-a6e9-4fd7-984c-dd90ebe49005",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b4d3f77-d7a1-4fd7-9e48-97fd46264867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f439eef-a6e9-4fd7-984c-dd90ebe49005",
                    "LayerId": "f9c47a3a-54eb-4c20-bcd3-7f1dc809184d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f9c47a3a-54eb-4c20-bcd3-7f1dc809184d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5659f7e7-1a90-40f6-a90e-06ec46b6bb44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}