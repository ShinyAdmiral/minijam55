{
    "id": "3cfbc67c-ca07-4eff-a080-af9c4b1e7e94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_painting1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffa659c4-d724-4b43-bcea-eccd681b05bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cfbc67c-ca07-4eff-a080-af9c4b1e7e94",
            "compositeImage": {
                "id": "8ea68077-21bc-430b-8aef-d3fa441a79cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa659c4-d724-4b43-bcea-eccd681b05bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db84c0f-597a-43ad-a935-b943b525459f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa659c4-d724-4b43-bcea-eccd681b05bc",
                    "LayerId": "fc2e6d5f-16e2-4bb6-9f97-745bf0a22aa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fc2e6d5f-16e2-4bb6-9f97-745bf0a22aa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cfbc67c-ca07-4eff-a080-af9c4b1e7e94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}