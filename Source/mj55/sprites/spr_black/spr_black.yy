{
    "id": "27eeade0-444c-4aa1-99ca-0c74a5ce78f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87a51bb1-c230-4e7f-8420-188048592755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27eeade0-444c-4aa1-99ca-0c74a5ce78f6",
            "compositeImage": {
                "id": "d3b1152d-2245-498b-b7d3-344b204f20f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87a51bb1-c230-4e7f-8420-188048592755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bde02b0-a421-49fb-8a88-38bd20624374",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87a51bb1-c230-4e7f-8420-188048592755",
                    "LayerId": "4384a3fe-fb78-4a30-a0b4-e3faf01bd16c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4384a3fe-fb78-4a30-a0b4-e3faf01bd16c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27eeade0-444c-4aa1-99ca-0c74a5ce78f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}