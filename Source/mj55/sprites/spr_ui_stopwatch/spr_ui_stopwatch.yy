{
    "id": "06164ce5-4146-4b79-b10c-f60e03829c4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_stopwatch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ccc94db-ea69-464e-84d6-434b72bae6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06164ce5-4146-4b79-b10c-f60e03829c4e",
            "compositeImage": {
                "id": "12a247bf-041d-4cca-b557-ac569e14a77f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ccc94db-ea69-464e-84d6-434b72bae6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133537e3-2e00-4f7c-a5f9-355a1d7701d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ccc94db-ea69-464e-84d6-434b72bae6c7",
                    "LayerId": "8e1c82a6-0417-45c6-bd3f-cd5365b2c58f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e1c82a6-0417-45c6-bd3f-cd5365b2c58f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06164ce5-4146-4b79-b10c-f60e03829c4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}