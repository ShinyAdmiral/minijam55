{
    "id": "36cfe8bb-f88b-477a-a197-efbd20e5c1b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_creditson",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 245,
    "bbox_right": 315,
    "bbox_top": 214,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32825eff-fcdc-4d34-bdb9-b9483e1679c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36cfe8bb-f88b-477a-a197-efbd20e5c1b0",
            "compositeImage": {
                "id": "c9aa2c18-4c4b-4d2b-9816-c38d488328c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32825eff-fcdc-4d34-bdb9-b9483e1679c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48eafea-204e-4d67-981f-5b4fae7f344d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32825eff-fcdc-4d34-bdb9-b9483e1679c2",
                    "LayerId": "368b2944-7c07-49f7-aaca-1583f162c245"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "368b2944-7c07-49f7-aaca-1583f162c245",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36cfe8bb-f88b-477a-a197-efbd20e5c1b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}