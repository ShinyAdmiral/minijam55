{
    "id": "2175496e-7b39-452d-824a-d79f79fd08d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_statue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 228,
    "bbox_right": 263,
    "bbox_top": 105,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbf1e10f-67f2-452d-b553-3f584a80ac53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2175496e-7b39-452d-824a-d79f79fd08d7",
            "compositeImage": {
                "id": "01bccc9c-cd85-4dbc-8b8e-51d9c053cad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf1e10f-67f2-452d-b553-3f584a80ac53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6742f6bb-817b-4a05-ba0d-1af6fad683df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf1e10f-67f2-452d-b553-3f584a80ac53",
                    "LayerId": "7dcb3166-6d0c-44ed-a28c-2146ed6e2aaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "7dcb3166-6d0c-44ed-a28c-2146ed6e2aaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2175496e-7b39-452d-824a-d79f79fd08d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}