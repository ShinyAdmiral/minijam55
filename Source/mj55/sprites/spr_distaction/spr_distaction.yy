{
    "id": "1e8015ca-779b-4732-a824-668beafa6c80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_distaction",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "621cf1c8-06e1-4b13-ad9c-55312db8a283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8015ca-779b-4732-a824-668beafa6c80",
            "compositeImage": {
                "id": "8e70dd10-7b93-4c98-b7e8-eef25589edde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "621cf1c8-06e1-4b13-ad9c-55312db8a283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3f7d86-ad58-4ed4-ac00-97442acadb75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "621cf1c8-06e1-4b13-ad9c-55312db8a283",
                    "LayerId": "8b1b6d5a-cb68-41b7-8e9f-1ca44e1a7ddc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8b1b6d5a-cb68-41b7-8e9f-1ca44e1a7ddc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e8015ca-779b-4732-a824-668beafa6c80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}