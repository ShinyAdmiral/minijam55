{
    "id": "74cd92cf-d251-4aef-94b8-ceb0d596e5d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_box_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 1,
    "bbox_right": 37,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9038844f-881a-4cba-9b59-5349880f8fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74cd92cf-d251-4aef-94b8-ceb0d596e5d0",
            "compositeImage": {
                "id": "cde3038f-027e-4101-8e81-2034c516de8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9038844f-881a-4cba-9b59-5349880f8fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76a179bb-b607-4ce5-b570-d9dd696b648b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9038844f-881a-4cba-9b59-5349880f8fae",
                    "LayerId": "b5bc9ebb-d45e-4c78-ac5a-efad2949137c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "b5bc9ebb-d45e-4c78-ac5a-efad2949137c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74cd92cf-d251-4aef-94b8-ceb0d596e5d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}