{
    "id": "16fa50a4-b7fb-4a6c-8076-6fcee74c45da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_circle_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8fdc97aa-7819-465a-b4e6-07a3757e5d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fa50a4-b7fb-4a6c-8076-6fcee74c45da",
            "compositeImage": {
                "id": "a8c1e30d-3778-4baa-bce5-3c1634e2a81d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fdc97aa-7819-465a-b4e6-07a3757e5d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ae716ef-1382-4f03-9f73-c88071aabeb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fdc97aa-7819-465a-b4e6-07a3757e5d41",
                    "LayerId": "d0994b8f-dbd0-4102-8ec7-2dc924c1276b"
                }
            ]
        },
        {
            "id": "f4653d9a-aa33-4f70-9277-f94dac18c693",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16fa50a4-b7fb-4a6c-8076-6fcee74c45da",
            "compositeImage": {
                "id": "bb22267a-b4f6-431b-9719-a23ad9325ec8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4653d9a-aa33-4f70-9277-f94dac18c693",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed11e58-bbef-4a29-a0c6-f3e0ee45b2b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4653d9a-aa33-4f70-9277-f94dac18c693",
                    "LayerId": "d0994b8f-dbd0-4102-8ec7-2dc924c1276b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0994b8f-dbd0-4102-8ec7-2dc924c1276b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16fa50a4-b7fb-4a6c-8076-6fcee74c45da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}