{
    "id": "d72ea883-6755-4bb1-be88-d440eb171050",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_ver_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0612cb41-3c7a-4812-850d-d1bca79ff7ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d72ea883-6755-4bb1-be88-d440eb171050",
            "compositeImage": {
                "id": "792a4f8c-72a3-44d1-a2a3-171fadadf795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0612cb41-3c7a-4812-850d-d1bca79ff7ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba6a48ca-c7fe-49cb-b96b-050703f97f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0612cb41-3c7a-4812-850d-d1bca79ff7ea",
                    "LayerId": "ccb1e478-97fb-4bed-ae46-ec5ffeb92231"
                }
            ]
        },
        {
            "id": "2a5e5632-bad6-4b27-a9eb-c2c7631eb6c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d72ea883-6755-4bb1-be88-d440eb171050",
            "compositeImage": {
                "id": "056bf3e8-84f0-4d03-afe3-a7440545a22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a5e5632-bad6-4b27-a9eb-c2c7631eb6c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65386e4b-ace6-4f56-a2b0-b5e5cbbf0a98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a5e5632-bad6-4b27-a9eb-c2c7631eb6c8",
                    "LayerId": "ccb1e478-97fb-4bed-ae46-ec5ffeb92231"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ccb1e478-97fb-4bed-ae46-ec5ffeb92231",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d72ea883-6755-4bb1-be88-d440eb171050",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}