{
    "id": "fa4ecc0c-c319-45d1-acb4-8446ce68446a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_painting_chicken",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d5c2062-076d-4f43-8d39-8aa96b17d204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa4ecc0c-c319-45d1-acb4-8446ce68446a",
            "compositeImage": {
                "id": "2dda2099-33ec-449e-b433-e45e8d399f36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d5c2062-076d-4f43-8d39-8aa96b17d204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e9b128-829c-4f28-bdce-0d715bfc4f18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d5c2062-076d-4f43-8d39-8aa96b17d204",
                    "LayerId": "345494fe-3562-4089-8f9e-1bc0a0f1ac69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "345494fe-3562-4089-8f9e-1bc0a0f1ac69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa4ecc0c-c319-45d1-acb4-8446ce68446a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}