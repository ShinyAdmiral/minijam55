{
    "id": "b3cbe907-a85e-4161-a0d5-a766a0a40920",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guard_rotater_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8074ff4-1409-4cd5-9cd9-a97bed4cab92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3cbe907-a85e-4161-a0d5-a766a0a40920",
            "compositeImage": {
                "id": "f2e484c8-6810-4d12-9f84-c243f1626dc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8074ff4-1409-4cd5-9cd9-a97bed4cab92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c62ec55-c6c6-40fb-aed5-2a2d3b059a12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8074ff4-1409-4cd5-9cd9-a97bed4cab92",
                    "LayerId": "700536e0-27a2-4c45-9be8-1a146f7ba635"
                }
            ]
        },
        {
            "id": "69b59b16-7009-46f9-9ef8-a8e2f418b0e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3cbe907-a85e-4161-a0d5-a766a0a40920",
            "compositeImage": {
                "id": "3425a7ec-43e8-4e8b-9eba-859a940c2618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69b59b16-7009-46f9-9ef8-a8e2f418b0e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6908c89d-92a7-4286-af81-278895b92401",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69b59b16-7009-46f9-9ef8-a8e2f418b0e8",
                    "LayerId": "700536e0-27a2-4c45-9be8-1a146f7ba635"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "700536e0-27a2-4c45-9be8-1a146f7ba635",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3cbe907-a85e-4161-a0d5-a766a0a40920",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}