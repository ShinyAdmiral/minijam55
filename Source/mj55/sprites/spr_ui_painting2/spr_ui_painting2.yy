{
    "id": "f0629428-93d9-4c0f-9336-1eabeb52be13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_painting2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "033179f6-73c5-433b-8f51-b8186bd86255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0629428-93d9-4c0f-9336-1eabeb52be13",
            "compositeImage": {
                "id": "167f5c39-29a9-4571-862c-5b74448f411f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033179f6-73c5-433b-8f51-b8186bd86255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f76f71-8106-4f9f-a03f-33f8f305ac36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033179f6-73c5-433b-8f51-b8186bd86255",
                    "LayerId": "16d2e147-342f-48d4-98e5-efe600158319"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "16d2e147-342f-48d4-98e5-efe600158319",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0629428-93d9-4c0f-9336-1eabeb52be13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}