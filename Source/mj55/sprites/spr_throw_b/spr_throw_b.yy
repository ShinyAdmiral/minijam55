{
    "id": "3a3cdb26-55f7-49a3-ab6b-097d27a052fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_throw_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 7,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5a9cf33-29f8-4673-b0cb-b8349e88da9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a3cdb26-55f7-49a3-ab6b-097d27a052fb",
            "compositeImage": {
                "id": "c7c3757c-1f00-4de6-aba2-3276a6feef01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5a9cf33-29f8-4673-b0cb-b8349e88da9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79aefe5f-84b8-42e3-bd67-3372574ff6b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5a9cf33-29f8-4673-b0cb-b8349e88da9e",
                    "LayerId": "b6995d25-c1e0-4b15-8d86-ff5f27574e9a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b6995d25-c1e0-4b15-8d86-ff5f27574e9a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a3cdb26-55f7-49a3-ab6b-097d27a052fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}