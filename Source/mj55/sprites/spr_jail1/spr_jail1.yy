{
    "id": "570a6b19-e404-4fc8-9c74-e329c6abb623",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jail1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 203,
    "bbox_right": 272,
    "bbox_top": 134,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e552623-4e2b-4880-aaf0-03fbac9efc3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570a6b19-e404-4fc8-9c74-e329c6abb623",
            "compositeImage": {
                "id": "29cea6a4-ff8d-4618-8978-1b4ac7ace2f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e552623-4e2b-4880-aaf0-03fbac9efc3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9927a3-08b5-49d6-ba3d-d3ac52f051bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e552623-4e2b-4880-aaf0-03fbac9efc3a",
                    "LayerId": "4b4ed713-f978-4ad5-b6eb-841955665988"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "4b4ed713-f978-4ad5-b6eb-841955665988",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "570a6b19-e404-4fc8-9c74-e329c6abb623",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}