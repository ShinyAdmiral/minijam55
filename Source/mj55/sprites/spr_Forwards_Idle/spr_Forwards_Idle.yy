{
    "id": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Forwards_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97ff2902-11b5-4dc2-804a-6859fa845974",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
            "compositeImage": {
                "id": "2c6ec309-5dd8-4509-842e-015d0102b631",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97ff2902-11b5-4dc2-804a-6859fa845974",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5529ee07-1ca1-4a21-9a23-11678a50e8b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97ff2902-11b5-4dc2-804a-6859fa845974",
                    "LayerId": "190cac4b-1917-4ec0-a46b-79dba78fe759"
                }
            ]
        },
        {
            "id": "d56590dc-e7f4-4b55-a4d0-1945466c9099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
            "compositeImage": {
                "id": "2f7488d6-7432-46e9-9e91-478789273caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d56590dc-e7f4-4b55-a4d0-1945466c9099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f667b9d1-ef7d-4289-b0bf-f45d1f89c2c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d56590dc-e7f4-4b55-a4d0-1945466c9099",
                    "LayerId": "190cac4b-1917-4ec0-a46b-79dba78fe759"
                }
            ]
        },
        {
            "id": "85b9d434-eb12-453b-b3c4-7acd49f9c3cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
            "compositeImage": {
                "id": "b1d887af-1dd6-4170-b05b-8997c8a7101b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b9d434-eb12-453b-b3c4-7acd49f9c3cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f198dc44-9795-45a8-83d9-398eea9a8e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b9d434-eb12-453b-b3c4-7acd49f9c3cf",
                    "LayerId": "190cac4b-1917-4ec0-a46b-79dba78fe759"
                }
            ]
        },
        {
            "id": "061b90a5-717a-46d0-b1f5-c5f4b3e7bca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
            "compositeImage": {
                "id": "340f2e99-c13c-460d-805e-0abd826bf52c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061b90a5-717a-46d0-b1f5-c5f4b3e7bca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c3f94b7-2067-4041-b030-6d891126ebc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061b90a5-717a-46d0-b1f5-c5f4b3e7bca0",
                    "LayerId": "190cac4b-1917-4ec0-a46b-79dba78fe759"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "190cac4b-1917-4ec0-a46b-79dba78fe759",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}