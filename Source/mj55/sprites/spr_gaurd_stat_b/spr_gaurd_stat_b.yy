{
    "id": "f24e3683-07b5-49aa-a8a0-bcd838504320",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_stat_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3279504-0ec0-4f5f-a885-35f677a2ddc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f24e3683-07b5-49aa-a8a0-bcd838504320",
            "compositeImage": {
                "id": "6b57a8b9-2bbf-4855-b0df-2268f41c2191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3279504-0ec0-4f5f-a885-35f677a2ddc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f424fac6-b4be-4e16-9c23-87661b22c988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3279504-0ec0-4f5f-a885-35f677a2ddc9",
                    "LayerId": "861afa0c-9de9-488f-991e-b54f3cd503cf"
                }
            ]
        },
        {
            "id": "283445ec-1250-4ef4-b0ae-2534c29006ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f24e3683-07b5-49aa-a8a0-bcd838504320",
            "compositeImage": {
                "id": "73b2482f-c076-42a0-930d-0b7137dd056c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "283445ec-1250-4ef4-b0ae-2534c29006ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b917bfd-2857-48ba-84f9-494b6d01a837",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "283445ec-1250-4ef4-b0ae-2534c29006ac",
                    "LayerId": "861afa0c-9de9-488f-991e-b54f3cd503cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "861afa0c-9de9-488f-991e-b54f3cd503cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f24e3683-07b5-49aa-a8a0-bcd838504320",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}