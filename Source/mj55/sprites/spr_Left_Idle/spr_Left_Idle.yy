{
    "id": "8ae8a4ab-df3b-4024-ba35-cb29179cc40b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Left_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 7,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36a6ed17-0eec-4842-8ebf-ed4eb4c6c44d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ae8a4ab-df3b-4024-ba35-cb29179cc40b",
            "compositeImage": {
                "id": "3c41a0ca-0000-488e-a9f8-f54aa990f95a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a6ed17-0eec-4842-8ebf-ed4eb4c6c44d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "021f830a-3352-4509-bf46-159b20405082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a6ed17-0eec-4842-8ebf-ed4eb4c6c44d",
                    "LayerId": "971d7131-25f5-4d00-8b9c-543818b9560f"
                }
            ]
        },
        {
            "id": "f6734df5-bd85-43f5-bf28-6efe2ba227a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ae8a4ab-df3b-4024-ba35-cb29179cc40b",
            "compositeImage": {
                "id": "486003ab-7361-483c-a65d-e11ecead581b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6734df5-bd85-43f5-bf28-6efe2ba227a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d817956-8215-4b33-af63-f4b9f1eef245",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6734df5-bd85-43f5-bf28-6efe2ba227a5",
                    "LayerId": "971d7131-25f5-4d00-8b9c-543818b9560f"
                }
            ]
        },
        {
            "id": "2cbef53e-fcdf-498c-afbf-e26133b9ab46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ae8a4ab-df3b-4024-ba35-cb29179cc40b",
            "compositeImage": {
                "id": "618b012d-c331-4f65-9a36-04082c411219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbef53e-fcdf-498c-afbf-e26133b9ab46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "362267f2-27a7-4d87-80a6-0a1f4f662e7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbef53e-fcdf-498c-afbf-e26133b9ab46",
                    "LayerId": "971d7131-25f5-4d00-8b9c-543818b9560f"
                }
            ]
        },
        {
            "id": "2598e48d-937c-45ef-b4a4-ceaccb3e1ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ae8a4ab-df3b-4024-ba35-cb29179cc40b",
            "compositeImage": {
                "id": "5a185dfc-d51c-4129-a606-0980322c9ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2598e48d-937c-45ef-b4a4-ceaccb3e1ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "062639ae-6aa8-4dbd-85e0-42a4a66d81e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2598e48d-937c-45ef-b4a4-ceaccb3e1ca9",
                    "LayerId": "971d7131-25f5-4d00-8b9c-543818b9560f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "971d7131-25f5-4d00-8b9c-543818b9560f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ae8a4ab-df3b-4024-ba35-cb29179cc40b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}