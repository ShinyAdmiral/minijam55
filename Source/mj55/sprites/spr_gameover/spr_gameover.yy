{
    "id": "086fe95d-d183-45e0-b512-16cec6bedb79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 166,
    "bbox_right": 301,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db7b050f-b73d-4b0f-ae78-02473720f332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "086fe95d-d183-45e0-b512-16cec6bedb79",
            "compositeImage": {
                "id": "67063fa9-cd36-4c7b-8186-f5f28b608b5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7b050f-b73d-4b0f-ae78-02473720f332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d8ad6e8-cbbe-4943-b2da-fbe92f351888",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7b050f-b73d-4b0f-ae78-02473720f332",
                    "LayerId": "4b629e5a-cc84-4b8c-ac93-4fd353105306"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "4b629e5a-cc84-4b8c-ac93-4fd353105306",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "086fe95d-d183-45e0-b512-16cec6bedb79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}