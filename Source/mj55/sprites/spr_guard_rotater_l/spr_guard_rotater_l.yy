{
    "id": "f33e1b39-675a-444b-9a9b-99946d8f2c2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guard_rotater_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "705b0f1d-84f5-4c84-be16-51c8719c9525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33e1b39-675a-444b-9a9b-99946d8f2c2e",
            "compositeImage": {
                "id": "a2f18563-316a-47c5-a8a6-9b6934d0a00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705b0f1d-84f5-4c84-be16-51c8719c9525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fa7bb25-ceca-4a60-836d-4a64250c656c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705b0f1d-84f5-4c84-be16-51c8719c9525",
                    "LayerId": "7ac92ff3-7e1d-43d2-81e4-43914df1c1f9"
                }
            ]
        },
        {
            "id": "c8b9052d-5179-4334-8661-822064a015f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f33e1b39-675a-444b-9a9b-99946d8f2c2e",
            "compositeImage": {
                "id": "9788e20a-4a57-4af8-a2b7-cb0de1dd8f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8b9052d-5179-4334-8661-822064a015f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a55defb-cd0c-4394-98cb-4731994c598d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8b9052d-5179-4334-8661-822064a015f6",
                    "LayerId": "7ac92ff3-7e1d-43d2-81e4-43914df1c1f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7ac92ff3-7e1d-43d2-81e4-43914df1c1f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f33e1b39-675a-444b-9a9b-99946d8f2c2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}