{
    "id": "e15592e5-63d0-4c5f-9d8d-0f9ad0413233",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ebb8a17-1b86-4109-9774-73f75cbd4fea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e15592e5-63d0-4c5f-9d8d-0f9ad0413233",
            "compositeImage": {
                "id": "6d1fef0c-adfe-4bf8-ac4d-15a2e746779b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ebb8a17-1b86-4109-9774-73f75cbd4fea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c3e45f-1ae3-4c4e-847b-5c35f67a93a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ebb8a17-1b86-4109-9774-73f75cbd4fea",
                    "LayerId": "e7fce0fd-2f57-4992-ba11-edf5d40296fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e7fce0fd-2f57-4992-ba11-edf5d40296fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e15592e5-63d0-4c5f-9d8d-0f9ad0413233",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}