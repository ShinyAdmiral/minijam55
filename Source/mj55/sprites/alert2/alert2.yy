{
    "id": "7ed325cb-6a79-4278-938e-0ea0ef433838",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "alert2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 9,
    "bbox_right": 20,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b5c81c0-8da9-4b49-b91e-b8bddc8bfd73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ed325cb-6a79-4278-938e-0ea0ef433838",
            "compositeImage": {
                "id": "d08d27c0-7e5e-4dd6-ba3e-177cdff07f0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5c81c0-8da9-4b49-b91e-b8bddc8bfd73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "099f3bb4-72a7-419b-a65f-cd6fa30fcef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5c81c0-8da9-4b49-b91e-b8bddc8bfd73",
                    "LayerId": "97cc8237-680a-4798-baf8-080249ddf1e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "97cc8237-680a-4798-baf8-080249ddf1e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ed325cb-6a79-4278-938e-0ea0ef433838",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}