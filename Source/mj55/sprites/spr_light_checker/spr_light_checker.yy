{
    "id": "8b8e08f2-28f5-4333-b979-8dfd12ca8eb5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light_checker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f09a5e27-28b8-4504-b24a-db4eea0a119e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b8e08f2-28f5-4333-b979-8dfd12ca8eb5",
            "compositeImage": {
                "id": "bfb11fdf-11a0-416c-9872-3ffc89f846a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f09a5e27-28b8-4504-b24a-db4eea0a119e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe658b65-4c36-4f7e-927e-384c14566777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f09a5e27-28b8-4504-b24a-db4eea0a119e",
                    "LayerId": "ffc2bb79-a0a0-446a-8bde-b38003ba24b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ffc2bb79-a0a0-446a-8bde-b38003ba24b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b8e08f2-28f5-4333-b979-8dfd12ca8eb5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}