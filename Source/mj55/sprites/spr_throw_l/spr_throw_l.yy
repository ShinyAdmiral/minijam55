{
    "id": "b70e41f7-7283-4077-94de-5c66b24668a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_throw_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "357c944a-16e6-4d20-a000-7740e43ac192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b70e41f7-7283-4077-94de-5c66b24668a7",
            "compositeImage": {
                "id": "4aca61d3-7f58-4b33-8717-343baa497cd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "357c944a-16e6-4d20-a000-7740e43ac192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5f09c70-f330-4ad7-9881-8d0113e3fadf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "357c944a-16e6-4d20-a000-7740e43ac192",
                    "LayerId": "f4d69917-201c-4e8a-a2af-afac829b9b8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f4d69917-201c-4e8a-a2af-afac829b9b8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b70e41f7-7283-4077-94de-5c66b24668a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}