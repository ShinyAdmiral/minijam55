{
    "id": "3fcbc1ab-1cee-4aa1-9cd2-66007780259e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_painting_ricardo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0159bd0f-33c4-489f-a59d-f36680fdfd48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fcbc1ab-1cee-4aa1-9cd2-66007780259e",
            "compositeImage": {
                "id": "5832fc43-903e-4aaa-8ce6-419514b90c9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0159bd0f-33c4-489f-a59d-f36680fdfd48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1684bcf8-148b-447f-9ece-4fb1c31c2b5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0159bd0f-33c4-489f-a59d-f36680fdfd48",
                    "LayerId": "5374c81c-735e-4fc9-98e3-0a91c3769db0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5374c81c-735e-4fc9-98e3-0a91c3769db0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fcbc1ab-1cee-4aa1-9cd2-66007780259e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}