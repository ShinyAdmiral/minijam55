{
    "id": "539ef954-0909-49c9-b280-603f8feb8043",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guard_rotater_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3db471ed-f6f4-4e67-9484-43eef497388d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "539ef954-0909-49c9-b280-603f8feb8043",
            "compositeImage": {
                "id": "ec3757ab-0f68-40f5-be68-8a94863c2d49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3db471ed-f6f4-4e67-9484-43eef497388d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be26ebda-2d80-4994-bf46-04e08e0d85b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3db471ed-f6f4-4e67-9484-43eef497388d",
                    "LayerId": "3af5ecb8-b930-43d9-9c5e-37789f44db7a"
                }
            ]
        },
        {
            "id": "42b5c46e-4d18-4354-8e23-cf3bfd1d40db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "539ef954-0909-49c9-b280-603f8feb8043",
            "compositeImage": {
                "id": "8ebbd5b2-73aa-490d-a1ab-2e361f7f6bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b5c46e-4d18-4354-8e23-cf3bfd1d40db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e126f542-ca32-4b87-8182-b74dbcf51534",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b5c46e-4d18-4354-8e23-cf3bfd1d40db",
                    "LayerId": "3af5ecb8-b930-43d9-9c5e-37789f44db7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3af5ecb8-b930-43d9-9c5e-37789f44db7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "539ef954-0909-49c9-b280-603f8feb8043",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}