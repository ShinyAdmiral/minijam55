{
    "id": "ebf117c7-3bf5-4853-b805-572dafcdc81b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Right_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee1162ed-42ec-4db2-8ae8-5e146b83ca09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf117c7-3bf5-4853-b805-572dafcdc81b",
            "compositeImage": {
                "id": "bc6c0b4a-4f35-46c0-a4cb-72e73ed03a0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee1162ed-42ec-4db2-8ae8-5e146b83ca09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8beb630a-6afb-4ce3-ba0c-8e01284f292e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee1162ed-42ec-4db2-8ae8-5e146b83ca09",
                    "LayerId": "b033df33-7380-4361-a86f-1e8038830222"
                }
            ]
        },
        {
            "id": "e3369824-9e00-4d36-83e3-ee3a88830963",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf117c7-3bf5-4853-b805-572dafcdc81b",
            "compositeImage": {
                "id": "17da6bfc-9cd2-4140-81af-0c1f316aeb70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3369824-9e00-4d36-83e3-ee3a88830963",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26ce4d09-fc93-4969-b46c-e1ce43080642",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3369824-9e00-4d36-83e3-ee3a88830963",
                    "LayerId": "b033df33-7380-4361-a86f-1e8038830222"
                }
            ]
        },
        {
            "id": "e2398271-8d6e-4988-82d9-a12a548eb88d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf117c7-3bf5-4853-b805-572dafcdc81b",
            "compositeImage": {
                "id": "8da43cdd-ced8-41fa-a362-1e0948d671d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2398271-8d6e-4988-82d9-a12a548eb88d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6de7b5c-bc42-4bd8-9a87-aa900657a8db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2398271-8d6e-4988-82d9-a12a548eb88d",
                    "LayerId": "b033df33-7380-4361-a86f-1e8038830222"
                }
            ]
        },
        {
            "id": "7b58764e-5072-44ac-91b8-8109c1f72e37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebf117c7-3bf5-4853-b805-572dafcdc81b",
            "compositeImage": {
                "id": "9958ac37-3bb0-45ea-a103-e64ae0c29530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b58764e-5072-44ac-91b8-8109c1f72e37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec93bab-2137-4594-8972-e8c50d72fbc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b58764e-5072-44ac-91b8-8109c1f72e37",
                    "LayerId": "b033df33-7380-4361-a86f-1e8038830222"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b033df33-7380-4361-a86f-1e8038830222",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebf117c7-3bf5-4853-b805-572dafcdc81b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}