{
    "id": "c6fe5fa2-8bbf-4ed2-afbc-12385ca3e046",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_emitter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2ea59f9-9c59-46c3-873b-12faa1be0ad7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6fe5fa2-8bbf-4ed2-afbc-12385ca3e046",
            "compositeImage": {
                "id": "3e4fe73a-c13c-481e-93cb-567bfd445873",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2ea59f9-9c59-46c3-873b-12faa1be0ad7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bafb73-8e0a-434d-96c5-2e0dc93e0ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2ea59f9-9c59-46c3-873b-12faa1be0ad7",
                    "LayerId": "b2b994ce-e29e-47a0-b0e5-6ee7de477e95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b2b994ce-e29e-47a0-b0e5-6ee7de477e95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6fe5fa2-8bbf-4ed2-afbc-12385ca3e046",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}