{
    "id": "fc12c17b-0c2e-427a-b280-540c9358e529",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_stat_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5e484bb-e240-4cbe-b2fa-955005e91626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc12c17b-0c2e-427a-b280-540c9358e529",
            "compositeImage": {
                "id": "f7df4892-831d-420f-b41e-44e20380f931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5e484bb-e240-4cbe-b2fa-955005e91626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bce1dda3-ebda-4832-bd71-9163cd3f84b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5e484bb-e240-4cbe-b2fa-955005e91626",
                    "LayerId": "1633fece-fd12-4f48-97bf-75190e1d1327"
                }
            ]
        },
        {
            "id": "623b3891-172f-4395-ad84-9e16b4779f30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc12c17b-0c2e-427a-b280-540c9358e529",
            "compositeImage": {
                "id": "fb499d61-9c50-4b2e-a6e7-6fc00ece5515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623b3891-172f-4395-ad84-9e16b4779f30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c58a2a3e-b27e-4f85-ad00-c654e302bb11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623b3891-172f-4395-ad84-9e16b4779f30",
                    "LayerId": "1633fece-fd12-4f48-97bf-75190e1d1327"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1633fece-fd12-4f48-97bf-75190e1d1327",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc12c17b-0c2e-427a-b280-540c9358e529",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}