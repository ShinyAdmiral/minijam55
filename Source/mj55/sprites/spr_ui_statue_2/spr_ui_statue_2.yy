{
    "id": "1b6f0e8e-ff7a-4142-a872-a30c168dcdba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_statue_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "decb1d4d-f2a0-48a3-b2a7-eda551d42554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b6f0e8e-ff7a-4142-a872-a30c168dcdba",
            "compositeImage": {
                "id": "88b9c199-1a6f-41b1-ab5a-09fcd837a065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "decb1d4d-f2a0-48a3-b2a7-eda551d42554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e46f62-03e2-4c74-a0cc-ea2c4ae8deac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "decb1d4d-f2a0-48a3-b2a7-eda551d42554",
                    "LayerId": "d426da72-5df8-4001-b669-5acbe0a4ac1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d426da72-5df8-4001-b669-5acbe0a4ac1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b6f0e8e-ff7a-4142-a872-a30c168dcdba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}