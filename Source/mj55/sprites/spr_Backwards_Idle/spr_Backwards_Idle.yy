{
    "id": "e6e2fb36-6000-4fc3-8c2e-86db3cc6a5c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Backwards_Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59544b1b-65e8-4ecf-9974-f7b3d013029f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e2fb36-6000-4fc3-8c2e-86db3cc6a5c0",
            "compositeImage": {
                "id": "2e3b65f1-cae1-4d15-83ea-e31251b19b3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59544b1b-65e8-4ecf-9974-f7b3d013029f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f2eda74-c1eb-4fdb-a125-22d43cc568e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59544b1b-65e8-4ecf-9974-f7b3d013029f",
                    "LayerId": "a323eaec-35a1-4c44-a146-e508af3b321a"
                }
            ]
        },
        {
            "id": "4f865149-558d-4d8a-8801-738b13729171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e2fb36-6000-4fc3-8c2e-86db3cc6a5c0",
            "compositeImage": {
                "id": "af351bfc-d20f-4693-8aac-b11b7bc559fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f865149-558d-4d8a-8801-738b13729171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d095098e-d7f8-4ba0-a573-0cd944b1858c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f865149-558d-4d8a-8801-738b13729171",
                    "LayerId": "a323eaec-35a1-4c44-a146-e508af3b321a"
                }
            ]
        },
        {
            "id": "3f2a880e-5e9f-4ccb-8bf9-de0c14553f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e2fb36-6000-4fc3-8c2e-86db3cc6a5c0",
            "compositeImage": {
                "id": "903ce574-8969-4fc5-be4a-36f3aa487f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f2a880e-5e9f-4ccb-8bf9-de0c14553f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "769d191b-9ae8-4ba9-b9ee-d266d315f3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f2a880e-5e9f-4ccb-8bf9-de0c14553f5f",
                    "LayerId": "a323eaec-35a1-4c44-a146-e508af3b321a"
                }
            ]
        },
        {
            "id": "f320464d-a2c3-4e61-b4c5-7d154172f9e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e2fb36-6000-4fc3-8c2e-86db3cc6a5c0",
            "compositeImage": {
                "id": "e53bcb92-7ab6-4f2f-8977-871fa61f1021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f320464d-a2c3-4e61-b4c5-7d154172f9e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd9cd352-90fb-4de7-b3a1-e322581b0fc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f320464d-a2c3-4e61-b4c5-7d154172f9e1",
                    "LayerId": "a323eaec-35a1-4c44-a146-e508af3b321a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a323eaec-35a1-4c44-a146-e508af3b321a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6e2fb36-6000-4fc3-8c2e-86db3cc6a5c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}