{
    "id": "eb003c4d-e78e-430d-b43d-6f98913796ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_stopper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac982ef1-330a-4a32-b9f2-e00bd4878336",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb003c4d-e78e-430d-b43d-6f98913796ca",
            "compositeImage": {
                "id": "c814e072-7388-47a6-874f-20b5740e76ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac982ef1-330a-4a32-b9f2-e00bd4878336",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f0183a9-c084-472e-ad09-19c8be4913cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac982ef1-330a-4a32-b9f2-e00bd4878336",
                    "LayerId": "62c5e90c-5a24-4699-88ce-3d8329cea5f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "62c5e90c-5a24-4699-88ce-3d8329cea5f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb003c4d-e78e-430d-b43d-6f98913796ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}