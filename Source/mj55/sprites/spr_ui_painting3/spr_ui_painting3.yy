{
    "id": "8832d5f3-d088-4ecf-819e-bbc596a3299d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_painting3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbb12f81-1de4-410c-89c3-0ddb10ec2de7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8832d5f3-d088-4ecf-819e-bbc596a3299d",
            "compositeImage": {
                "id": "82157020-0905-49ae-be7b-527c66b41bcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb12f81-1de4-410c-89c3-0ddb10ec2de7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f7f293c-509a-4c12-9dd0-144de49adec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb12f81-1de4-410c-89c3-0ddb10ec2de7",
                    "LayerId": "bc8c3895-7600-4be0-b582-adccfa97fc75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc8c3895-7600-4be0-b582-adccfa97fc75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8832d5f3-d088-4ecf-819e-bbc596a3299d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}