{
    "id": "e27ddf1e-3b2b-46dc-88dc-4e1eef91a49b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_painting_cursed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cc19f35-0bc5-4714-9ab6-b0b87bbb6da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e27ddf1e-3b2b-46dc-88dc-4e1eef91a49b",
            "compositeImage": {
                "id": "e630e967-4ce3-487d-bd7f-fdb28d84bff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc19f35-0bc5-4714-9ab6-b0b87bbb6da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "043f49e2-c40d-4fc1-9148-95daf1ac21a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc19f35-0bc5-4714-9ab6-b0b87bbb6da4",
                    "LayerId": "7b42f159-e683-434e-ac21-400ad6d0e572"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7b42f159-e683-434e-ac21-400ad6d0e572",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e27ddf1e-3b2b-46dc-88dc-4e1eef91a49b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}