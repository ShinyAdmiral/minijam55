{
    "id": "93d6c0dd-bfed-4ea2-a36f-68c1cdd28db9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_painting_vector",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "085ce348-40fb-47fa-a0b4-f97c5e076137",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93d6c0dd-bfed-4ea2-a36f-68c1cdd28db9",
            "compositeImage": {
                "id": "c8dad2ae-bdb1-4b38-9c43-afcaac87e35f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "085ce348-40fb-47fa-a0b4-f97c5e076137",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9fa3b9e-a834-4f50-95af-659190f63e83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "085ce348-40fb-47fa-a0b4-f97c5e076137",
                    "LayerId": "55986d7c-32fc-4203-88b7-20259d852102"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "55986d7c-32fc-4203-88b7-20259d852102",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93d6c0dd-bfed-4ea2-a36f-68c1cdd28db9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}