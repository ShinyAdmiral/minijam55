{
    "id": "1da3a7fb-38c7-4efa-81d4-7532a1f0bf27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_hor_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65a3facb-5710-434f-bd00-9a6650239410",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1da3a7fb-38c7-4efa-81d4-7532a1f0bf27",
            "compositeImage": {
                "id": "a3e0d3dc-e694-47d6-965e-8b88af09987e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a3facb-5710-434f-bd00-9a6650239410",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "791cd987-a210-40d3-b476-5dff8d3ba300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a3facb-5710-434f-bd00-9a6650239410",
                    "LayerId": "22269818-d3ee-4064-bf13-8de17d955b89"
                }
            ]
        },
        {
            "id": "10ddaa6f-258c-4f25-a500-11a27db4a3d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1da3a7fb-38c7-4efa-81d4-7532a1f0bf27",
            "compositeImage": {
                "id": "7b00bf36-6440-4baa-8181-32080feedc14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ddaa6f-258c-4f25-a500-11a27db4a3d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf49dbda-fe21-4dda-9214-474528fe0a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ddaa6f-258c-4f25-a500-11a27db4a3d9",
                    "LayerId": "22269818-d3ee-4064-bf13-8de17d955b89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "22269818-d3ee-4064-bf13-8de17d955b89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1da3a7fb-38c7-4efa-81d4-7532a1f0bf27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}