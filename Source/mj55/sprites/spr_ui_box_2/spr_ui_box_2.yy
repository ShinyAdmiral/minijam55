{
    "id": "2843d087-47e4-4d0b-a077-97dbd2085ec3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_box_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 1,
    "bbox_right": 37,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72c294f6-47b3-42df-ba36-67af444f4e24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2843d087-47e4-4d0b-a077-97dbd2085ec3",
            "compositeImage": {
                "id": "2262eaf3-7fe8-4727-8b40-3543357cf84b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c294f6-47b3-42df-ba36-67af444f4e24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ccf7d88-fecf-40bb-a1ad-52ab033dd131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c294f6-47b3-42df-ba36-67af444f4e24",
                    "LayerId": "0ba07c87-83cd-4bb1-89bc-6bda15523996"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0ba07c87-83cd-4bb1-89bc-6bda15523996",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2843d087-47e4-4d0b-a077-97dbd2085ec3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}