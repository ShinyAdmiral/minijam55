{
    "id": "97f9de2b-2fa7-4962-ab92-c712f64d47f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_circle_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d658319e-96ca-4e1a-a9cf-95a054e6a4aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97f9de2b-2fa7-4962-ab92-c712f64d47f8",
            "compositeImage": {
                "id": "7a69079d-6ed7-436f-9b13-b78cd7dd3dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d658319e-96ca-4e1a-a9cf-95a054e6a4aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cb08414-43bb-4a1b-85b1-648eb0bb1146",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d658319e-96ca-4e1a-a9cf-95a054e6a4aa",
                    "LayerId": "6df58ff1-a7c0-4486-8dc6-356707b40136"
                }
            ]
        },
        {
            "id": "443cb5c1-2066-406c-9ead-2ce9b4d3d28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97f9de2b-2fa7-4962-ab92-c712f64d47f8",
            "compositeImage": {
                "id": "6ecaa8cc-a97a-462e-a391-2088c75b9322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "443cb5c1-2066-406c-9ead-2ce9b4d3d28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "feba283c-f242-4e87-b05d-efd359bd10d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "443cb5c1-2066-406c-9ead-2ce9b4d3d28d",
                    "LayerId": "6df58ff1-a7c0-4486-8dc6-356707b40136"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6df58ff1-a7c0-4486-8dc6-356707b40136",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97f9de2b-2fa7-4962-ab92-c712f64d47f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}