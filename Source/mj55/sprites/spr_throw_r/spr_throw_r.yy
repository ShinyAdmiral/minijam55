{
    "id": "3a402dee-3d6d-4c16-9362-2b6f98b904f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_throw_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 6,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34a3ae8e-80e7-4419-b792-d3beca3139a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a402dee-3d6d-4c16-9362-2b6f98b904f6",
            "compositeImage": {
                "id": "a493290e-508b-4f6a-9e63-6542be75205e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34a3ae8e-80e7-4419-b792-d3beca3139a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbca4968-cad0-4ad0-a5d9-23117ee27d87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34a3ae8e-80e7-4419-b792-d3beca3139a8",
                    "LayerId": "ef59bf78-dc19-4057-9128-7b7c3f1016b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ef59bf78-dc19-4057-9128-7b7c3f1016b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a402dee-3d6d-4c16-9362-2b6f98b904f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}