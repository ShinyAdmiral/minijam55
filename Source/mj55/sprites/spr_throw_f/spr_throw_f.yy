{
    "id": "29215b00-aa73-4aa4-87c5-57c94d3e81e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_throw_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 7,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "026fe74c-01a7-4f43-b899-bb707d7e9c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29215b00-aa73-4aa4-87c5-57c94d3e81e7",
            "compositeImage": {
                "id": "8ebd6220-b5f8-4f1c-96e1-642e8583f005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026fe74c-01a7-4f43-b899-bb707d7e9c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b709f87-5784-4f9f-9283-333a4e3ca768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026fe74c-01a7-4f43-b899-bb707d7e9c47",
                    "LayerId": "378ce7d8-e605-46ea-89b1-9af4bc51d256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "378ce7d8-e605-46ea-89b1-9af4bc51d256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29215b00-aa73-4aa4-87c5-57c94d3e81e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}