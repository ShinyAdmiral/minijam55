{
    "id": "a3122e2b-26df-41d7-bea5-0058dc1abb6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 22,
    "bbox_right": 28,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ac69b8d-cc7f-4415-88a6-3f55c656f835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3122e2b-26df-41d7-bea5-0058dc1abb6e",
            "compositeImage": {
                "id": "01e425d9-5f6a-4281-ad56-2c6ec1c563ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac69b8d-cc7f-4415-88a6-3f55c656f835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2615dd50-77f6-4e36-afd0-cd50faa545dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac69b8d-cc7f-4415-88a6-3f55c656f835",
                    "LayerId": "1d1f3526-c577-41b3-b600-713b4f2a1f7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1d1f3526-c577-41b3-b600-713b4f2a1f7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3122e2b-26df-41d7-bea5-0058dc1abb6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}