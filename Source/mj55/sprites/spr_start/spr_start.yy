{
    "id": "10d79b79-0f65-46c6-9995-cf507303e2bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 210,
    "bbox_right": 280,
    "bbox_top": 188,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5b79d5e-1b02-41a7-b1f1-74e0eac9bf84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10d79b79-0f65-46c6-9995-cf507303e2bb",
            "compositeImage": {
                "id": "15827846-1438-496d-a9c8-372396e08ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5b79d5e-1b02-41a7-b1f1-74e0eac9bf84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b86ea7c6-0b4f-4275-9557-fe8256cba67c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5b79d5e-1b02-41a7-b1f1-74e0eac9bf84",
                    "LayerId": "9d32e28f-48e5-410f-88de-91705086f5a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "9d32e28f-48e5-410f-88de-91705086f5a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10d79b79-0f65-46c6-9995-cf507303e2bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}