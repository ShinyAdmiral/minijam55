{
    "id": "36895d98-fb68-4d70-8887-76923fc804d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_hor_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbd8845a-df35-43c1-a907-67a1c80a4c71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36895d98-fb68-4d70-8887-76923fc804d2",
            "compositeImage": {
                "id": "6ac29591-a127-4fd4-808f-4cfd52ff8147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd8845a-df35-43c1-a907-67a1c80a4c71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edfeaa45-08d3-4ab1-8b95-ed91617b3957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd8845a-df35-43c1-a907-67a1c80a4c71",
                    "LayerId": "fd8c1bcd-a481-4014-b8db-32cafe74766d"
                }
            ]
        },
        {
            "id": "856b297a-c68d-4c1e-9611-319de67a1ba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36895d98-fb68-4d70-8887-76923fc804d2",
            "compositeImage": {
                "id": "f834bf86-9395-4b2f-b30f-5cdaada4274a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "856b297a-c68d-4c1e-9611-319de67a1ba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57826660-84f1-4567-8dbb-358a631603be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "856b297a-c68d-4c1e-9611-319de67a1ba1",
                    "LayerId": "fd8c1bcd-a481-4014-b8db-32cafe74766d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fd8c1bcd-a481-4014-b8db-32cafe74766d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36895d98-fb68-4d70-8887-76923fc804d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}