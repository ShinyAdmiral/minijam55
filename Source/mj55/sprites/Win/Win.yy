{
    "id": "e75b61f7-61e1-44d9-9ae2-61d5fa5bb8b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Win",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2b18f38-71b0-43d1-94eb-d95aa7926de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e75b61f7-61e1-44d9-9ae2-61d5fa5bb8b8",
            "compositeImage": {
                "id": "9ebdd3aa-5499-4a9b-a8c5-9c4d4c563ddd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2b18f38-71b0-43d1-94eb-d95aa7926de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ca7bba6-7dd1-4074-a2cb-1d8d9316b7cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2b18f38-71b0-43d1-94eb-d95aa7926de3",
                    "LayerId": "8d19188e-436d-4db4-8315-8819c4a072ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "8d19188e-436d-4db4-8315-8819c4a072ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e75b61f7-61e1-44d9-9ae2-61d5fa5bb8b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}