{
    "id": "2be63fad-5fba-4623-98e9-b5b90f6a609e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "painting",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daaebcc3-3547-48b7-853e-41b8bdb34070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2be63fad-5fba-4623-98e9-b5b90f6a609e",
            "compositeImage": {
                "id": "69ad9fc2-59f9-4443-ac83-84c3f672db01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daaebcc3-3547-48b7-853e-41b8bdb34070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77635640-821c-4173-91c1-695bef945579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daaebcc3-3547-48b7-853e-41b8bdb34070",
                    "LayerId": "216185fa-f5ef-4e8b-9589-c67f15413514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "216185fa-f5ef-4e8b-9589-c67f15413514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2be63fad-5fba-4623-98e9-b5b90f6a609e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}