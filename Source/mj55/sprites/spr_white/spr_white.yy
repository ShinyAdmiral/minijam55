{
    "id": "fd7a500f-3bde-41f3-90f6-c79f817f3525",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "80908ec3-e519-4a3f-93dd-4cf9a6bdfb44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd7a500f-3bde-41f3-90f6-c79f817f3525",
            "compositeImage": {
                "id": "aea3c987-848f-4373-81b7-eb5b34602c88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80908ec3-e519-4a3f-93dd-4cf9a6bdfb44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84ff2fe-aa42-4c59-b0a4-961aa35bc656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80908ec3-e519-4a3f-93dd-4cf9a6bdfb44",
                    "LayerId": "e740032c-7093-41d6-8c65-e950422084b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e740032c-7093-41d6-8c65-e950422084b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd7a500f-3bde-41f3-90f6-c79f817f3525",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}