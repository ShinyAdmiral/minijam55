{
    "id": "ad9a5055-a669-4ddf-bc4e-833623239d20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_stat_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a133542d-bf69-4733-b3b8-3ffa8c74a623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad9a5055-a669-4ddf-bc4e-833623239d20",
            "compositeImage": {
                "id": "d9d2ab73-aaf8-4e6f-a3c6-7d692445f715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a133542d-bf69-4733-b3b8-3ffa8c74a623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2589dc80-d131-44c2-bc19-cdf32f2eb04c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a133542d-bf69-4733-b3b8-3ffa8c74a623",
                    "LayerId": "d90742c1-2bb7-4288-a0e7-f5d33857156a"
                }
            ]
        },
        {
            "id": "b9cd3963-710b-4c52-99a3-9ce10d49842a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad9a5055-a669-4ddf-bc4e-833623239d20",
            "compositeImage": {
                "id": "7fce9b7e-9683-45e9-b0ed-2363141fed66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9cd3963-710b-4c52-99a3-9ce10d49842a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac8334af-c81f-455b-b187-108f4bfce8f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9cd3963-710b-4c52-99a3-9ce10d49842a",
                    "LayerId": "d90742c1-2bb7-4288-a0e7-f5d33857156a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d90742c1-2bb7-4288-a0e7-f5d33857156a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad9a5055-a669-4ddf-bc4e-833623239d20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}