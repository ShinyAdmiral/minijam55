{
    "id": "6c2ccda6-431c-4b22-b968-7c1a2c9881e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_ver_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10123b5a-b60c-42c8-a653-551671f64d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2ccda6-431c-4b22-b968-7c1a2c9881e5",
            "compositeImage": {
                "id": "b4965992-e54e-4539-b566-bd52a2063556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10123b5a-b60c-42c8-a653-551671f64d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94ba86b-34ab-4561-9521-d2d224b16082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10123b5a-b60c-42c8-a653-551671f64d26",
                    "LayerId": "a63c817e-bf4e-4738-adee-f90fc3ed79b0"
                }
            ]
        },
        {
            "id": "4b6401c6-5e97-4d88-998d-89c40c6fb475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2ccda6-431c-4b22-b968-7c1a2c9881e5",
            "compositeImage": {
                "id": "0ae76f98-1e0e-4ba1-b692-6e38b7ff5aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6401c6-5e97-4d88-998d-89c40c6fb475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13c9d891-2883-4160-a192-89d825a09e71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6401c6-5e97-4d88-998d-89c40c6fb475",
                    "LayerId": "a63c817e-bf4e-4738-adee-f90fc3ed79b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a63c817e-bf4e-4738-adee-f90fc3ed79b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c2ccda6-431c-4b22-b968-7c1a2c9881e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}