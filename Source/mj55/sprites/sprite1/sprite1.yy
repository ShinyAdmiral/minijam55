{
    "id": "e9489669-1762-4985-83cb-94245d8d4162",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "372e00fa-8a4b-4b16-8ed9-5f98837f2403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9489669-1762-4985-83cb-94245d8d4162",
            "compositeImage": {
                "id": "241318b1-dbf7-49f8-9585-9a4b51d863b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "372e00fa-8a4b-4b16-8ed9-5f98837f2403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "613313d3-2d29-42b6-bad1-6e79bca4ec12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "372e00fa-8a4b-4b16-8ed9-5f98837f2403",
                    "LayerId": "355c83a3-b43c-4293-93de-688161caeb5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "355c83a3-b43c-4293-93de-688161caeb5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9489669-1762-4985-83cb-94245d8d4162",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}