{
    "id": "1b8fc974-3f43-40ce-bc76-7756bd1550dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_circle_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 5,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bd57a12-45a6-430b-8dc5-89c362e3a1ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b8fc974-3f43-40ce-bc76-7756bd1550dc",
            "compositeImage": {
                "id": "ddbe3b58-17c2-4e76-93ed-16bbfd47bc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd57a12-45a6-430b-8dc5-89c362e3a1ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06e8f169-aab8-4d25-9545-ce4c0d13302f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd57a12-45a6-430b-8dc5-89c362e3a1ff",
                    "LayerId": "3eb298a2-b8ec-45ad-9b50-e9b45aed15c3"
                }
            ]
        },
        {
            "id": "165c8b53-992f-4b88-bd70-62a087f7c964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b8fc974-3f43-40ce-bc76-7756bd1550dc",
            "compositeImage": {
                "id": "71fd4649-bbcf-43d8-b79a-e6c5fbcf6703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165c8b53-992f-4b88-bd70-62a087f7c964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d5ef0e-9e5a-43fc-92ad-a450178052ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165c8b53-992f-4b88-bd70-62a087f7c964",
                    "LayerId": "3eb298a2-b8ec-45ad-9b50-e9b45aed15c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3eb298a2-b8ec-45ad-9b50-e9b45aed15c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b8fc974-3f43-40ce-bc76-7756bd1550dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}