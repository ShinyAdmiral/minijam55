{
    "id": "aadad27a-d945-489c-ac30-30c5805864da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7919038-01ce-4c5a-acb5-6a6f6fae58e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aadad27a-d945-489c-ac30-30c5805864da",
            "compositeImage": {
                "id": "fc432444-3ef6-4ac7-98d2-4da5da0cebeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7919038-01ce-4c5a-acb5-6a6f6fae58e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e358f37-394d-4bf7-a4e2-9c755a12ac7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7919038-01ce-4c5a-acb5-6a6f6fae58e5",
                    "LayerId": "d1812dd2-8359-4290-8156-a46084c30d08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d1812dd2-8359-4290-8156-a46084c30d08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aadad27a-d945-489c-ac30-30c5805864da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}