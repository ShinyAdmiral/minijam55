{
    "id": "cef31bee-f388-49a6-87cc-417f7da5877c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 157,
    "bbox_right": 326,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad2c01ee-3edd-4e1d-a0b9-5114aad9f634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cef31bee-f388-49a6-87cc-417f7da5877c",
            "compositeImage": {
                "id": "c1c54cb9-ce61-43db-8eff-60aa0b032f28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2c01ee-3edd-4e1d-a0b9-5114aad9f634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7745ace3-0df9-4f32-8315-20827a5ae1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2c01ee-3edd-4e1d-a0b9-5114aad9f634",
                    "LayerId": "03a6f956-a260-42bf-9786-449aefab7548"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "03a6f956-a260-42bf-9786-449aefab7548",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cef31bee-f388-49a6-87cc-417f7da5877c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}