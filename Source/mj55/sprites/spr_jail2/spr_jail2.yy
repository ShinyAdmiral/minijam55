{
    "id": "88548e99-7f21-4d2c-9d3e-28f4bf122eaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jail2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 197,
    "bbox_left": 203,
    "bbox_right": 272,
    "bbox_top": 134,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab57811f-65ae-45bc-99a1-00ef28c2c21a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88548e99-7f21-4d2c-9d3e-28f4bf122eaa",
            "compositeImage": {
                "id": "9d69fe38-dd86-4351-97c5-8769ae9d02ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab57811f-65ae-45bc-99a1-00ef28c2c21a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea13ab8-0d38-4394-a8cb-13acc41f8852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab57811f-65ae-45bc-99a1-00ef28c2c21a",
                    "LayerId": "92cb859f-0cb2-4755-b366-d75d6b70809d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "92cb859f-0cb2-4755-b366-d75d6b70809d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88548e99-7f21-4d2c-9d3e-28f4bf122eaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}