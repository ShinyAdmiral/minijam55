{
    "id": "05e86093-dd3b-479b-a24e-2d3a3a88aeaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_paintingicon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ddfd697b-1458-4920-a854-77da2818e447",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05e86093-dd3b-479b-a24e-2d3a3a88aeaf",
            "compositeImage": {
                "id": "8a086433-b9b1-478c-83a6-b90152345e53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddfd697b-1458-4920-a854-77da2818e447",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3097d78-8d8e-45f0-83f8-aa2600b314d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddfd697b-1458-4920-a854-77da2818e447",
                    "LayerId": "52bcdbeb-dbbb-4ca2-a7db-6837b70eed16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "52bcdbeb-dbbb-4ca2-a7db-6837b70eed16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05e86093-dd3b-479b-a24e-2d3a3a88aeaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}