{
    "id": "7a6ff323-65f9-4555-8fc9-3c45e2cca075",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_right_on",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 22,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a292304d-1c0a-40c5-8525-8b46f789d9b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6ff323-65f9-4555-8fc9-3c45e2cca075",
            "compositeImage": {
                "id": "f8c175f2-8ee7-477f-873f-a6b63b581875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a292304d-1c0a-40c5-8525-8b46f789d9b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186098a4-b6ad-46bc-9f01-120fc0eecbae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a292304d-1c0a-40c5-8525-8b46f789d9b9",
                    "LayerId": "41de5d43-b0c4-473a-bc04-cf8791e55b82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "41de5d43-b0c4-473a-bc04-cf8791e55b82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a6ff323-65f9-4555-8fc9-3c45e2cca075",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}