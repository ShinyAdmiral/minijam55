{
    "id": "0ae50d78-db2f-4985-b800-94e65705418f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 245,
    "bbox_right": 315,
    "bbox_top": 214,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f81188d7-518f-49ed-be56-8640f485e066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ae50d78-db2f-4985-b800-94e65705418f",
            "compositeImage": {
                "id": "6dd0f263-6ef3-4bc1-85fd-51ac083ac213",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f81188d7-518f-49ed-be56-8640f485e066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47de0059-5e2d-4ece-a1da-5a02e582cb58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f81188d7-518f-49ed-be56-8640f485e066",
                    "LayerId": "acf22893-f058-4ae6-8f5d-f71eed157e08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "acf22893-f058-4ae6-8f5d-f71eed157e08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ae50d78-db2f-4985-b800-94e65705418f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}