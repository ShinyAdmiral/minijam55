{
    "id": "7a6d918c-717e-4ec9-8a8a-fe24d8925c17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_statue1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "728a4779-57aa-4992-b422-03b9ffc9d964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6d918c-717e-4ec9-8a8a-fe24d8925c17",
            "compositeImage": {
                "id": "c7854c6f-497e-4246-9c6a-b6c38fe50e3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "728a4779-57aa-4992-b422-03b9ffc9d964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1146abf-1403-4f85-b2ab-c926f7e16a8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "728a4779-57aa-4992-b422-03b9ffc9d964",
                    "LayerId": "bb54595c-1ea1-4777-90ab-815568b695c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bb54595c-1ea1-4777-90ab-815568b695c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a6d918c-717e-4ec9-8a8a-fe24d8925c17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}