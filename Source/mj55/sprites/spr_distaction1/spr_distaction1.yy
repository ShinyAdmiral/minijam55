{
    "id": "cd5d0840-e8eb-4bdc-ac3f-0b5019fbbb22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_distaction1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23fa94db-de4c-4dde-bd46-035d3acd91f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd5d0840-e8eb-4bdc-ac3f-0b5019fbbb22",
            "compositeImage": {
                "id": "d2096789-69d7-4aca-bff5-952d495c98cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23fa94db-de4c-4dde-bd46-035d3acd91f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a19eb8-1900-4aa6-b79c-01c287df9f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23fa94db-de4c-4dde-bd46-035d3acd91f7",
                    "LayerId": "4555adfe-5bd2-457d-b25b-5de60b841e77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4555adfe-5bd2-457d-b25b-5de60b841e77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd5d0840-e8eb-4bdc-ac3f-0b5019fbbb22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}