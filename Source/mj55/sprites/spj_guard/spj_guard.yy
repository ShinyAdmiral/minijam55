{
    "id": "9c3de360-d423-469f-bd95-55facf769a23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spj_guard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b0b6b03-a0a8-463d-ab68-05a821e31735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c3de360-d423-469f-bd95-55facf769a23",
            "compositeImage": {
                "id": "3284753c-4acf-480e-a400-d6cdb30defa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b0b6b03-a0a8-463d-ab68-05a821e31735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db8dcc97-76e8-44db-a760-e7a1e8cf2c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b0b6b03-a0a8-463d-ab68-05a821e31735",
                    "LayerId": "9a1a2d07-edbf-43a1-b748-e18aea7f71f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9a1a2d07-edbf-43a1-b748-e18aea7f71f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c3de360-d423-469f-bd95-55facf769a23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}