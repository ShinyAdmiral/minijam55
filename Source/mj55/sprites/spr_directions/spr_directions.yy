{
    "id": "9dc94eb4-615e-42ea-8589-dcaca77d4a5c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_directions",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 266,
    "bbox_left": 3,
    "bbox_right": 474,
    "bbox_top": 255,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a9fa350-ff5a-49ca-8555-ad348c4dd6ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dc94eb4-615e-42ea-8589-dcaca77d4a5c",
            "compositeImage": {
                "id": "a294a07b-c856-4625-871f-896b6f2d1e65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9fa350-ff5a-49ca-8555-ad348c4dd6ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "053fdcab-332c-4d55-941b-9c6784501829",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9fa350-ff5a-49ca-8555-ad348c4dd6ee",
                    "LayerId": "fe156e7d-6276-43d2-954d-2f8175952479"
                },
                {
                    "id": "a8f43b5a-cf21-4380-a4d5-107844e581eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9fa350-ff5a-49ca-8555-ad348c4dd6ee",
                    "LayerId": "3d183732-739e-4be6-a5d9-23bbdc3e54fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "fe156e7d-6276-43d2-954d-2f8175952479",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dc94eb4-615e-42ea-8589-dcaca77d4a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "3d183732-739e-4be6-a5d9-23bbdc3e54fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dc94eb4-615e-42ea-8589-dcaca77d4a5c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}