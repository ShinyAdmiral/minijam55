{
    "id": "af13e9b0-cf62-44f7-b968-a3f13248911d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_starton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 219,
    "bbox_left": 210,
    "bbox_right": 280,
    "bbox_top": 188,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbf2137e-8aad-4d3e-8105-6cc00f16f1d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af13e9b0-cf62-44f7-b968-a3f13248911d",
            "compositeImage": {
                "id": "f4893964-dd7c-44f1-b709-342ed3e0aff7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbf2137e-8aad-4d3e-8105-6cc00f16f1d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af2c4232-91e6-42a7-9091-f6870b3a2c47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbf2137e-8aad-4d3e-8105-6cc00f16f1d9",
                    "LayerId": "19088d94-c75c-4a9f-9c75-253f3da48564"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "19088d94-c75c-4a9f-9c75-253f3da48564",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af13e9b0-cf62-44f7-b968-a3f13248911d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}