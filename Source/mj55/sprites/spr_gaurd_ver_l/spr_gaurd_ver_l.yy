{
    "id": "0352dd88-ec2b-42a5-a6d3-7745b243c3d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_ver_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84168b6d-3b26-464c-b260-b3aa85751f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0352dd88-ec2b-42a5-a6d3-7745b243c3d0",
            "compositeImage": {
                "id": "a195b50d-774f-434d-883c-b179e3095708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84168b6d-3b26-464c-b260-b3aa85751f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b0a25c-1009-483a-9526-c35905d1db50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84168b6d-3b26-464c-b260-b3aa85751f87",
                    "LayerId": "9b15710e-d998-420a-af73-5be51513f242"
                }
            ]
        },
        {
            "id": "e6546108-d237-4643-9c0b-8379a250752a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0352dd88-ec2b-42a5-a6d3-7745b243c3d0",
            "compositeImage": {
                "id": "cbd88b56-a7f6-4e91-9f00-230ef3d3a10b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6546108-d237-4643-9c0b-8379a250752a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a970486-43a4-4e5c-9419-b1e55f8c5b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6546108-d237-4643-9c0b-8379a250752a",
                    "LayerId": "9b15710e-d998-420a-af73-5be51513f242"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9b15710e-d998-420a-af73-5be51513f242",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0352dd88-ec2b-42a5-a6d3-7745b243c3d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}