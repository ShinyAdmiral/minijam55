{
    "id": "2d2802cb-d379-4260-938b-38e70efb70d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_poof",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 7,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7d4676d-79aa-4021-a861-2349156a1fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2802cb-d379-4260-938b-38e70efb70d7",
            "compositeImage": {
                "id": "a4c0f9d4-4d27-4b84-b6c6-e2b33fca7ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d4676d-79aa-4021-a861-2349156a1fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30ea2775-7704-49d1-9c6e-14b3c4901e09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d4676d-79aa-4021-a861-2349156a1fb0",
                    "LayerId": "99c12767-723a-4e25-b485-45b3d1d547f4"
                }
            ]
        },
        {
            "id": "4b6f9bcf-ed46-46e3-9702-28f7b782b80a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2802cb-d379-4260-938b-38e70efb70d7",
            "compositeImage": {
                "id": "33438a79-7add-4a1e-bd26-ddb5d74dd84f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b6f9bcf-ed46-46e3-9702-28f7b782b80a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c9fadf3-9704-45c0-ae5a-12f3b1bb8671",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b6f9bcf-ed46-46e3-9702-28f7b782b80a",
                    "LayerId": "99c12767-723a-4e25-b485-45b3d1d547f4"
                }
            ]
        },
        {
            "id": "06dfb994-91cb-4bdb-a950-81673df088c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2802cb-d379-4260-938b-38e70efb70d7",
            "compositeImage": {
                "id": "e615e149-ba00-4b25-8bb7-d7d964fa150d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06dfb994-91cb-4bdb-a950-81673df088c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0697d67e-d4a4-4169-92be-373b0af98d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06dfb994-91cb-4bdb-a950-81673df088c7",
                    "LayerId": "99c12767-723a-4e25-b485-45b3d1d547f4"
                }
            ]
        },
        {
            "id": "10828654-7afb-4e36-b2ed-694e784d8830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d2802cb-d379-4260-938b-38e70efb70d7",
            "compositeImage": {
                "id": "68e6bb51-1f20-4c3f-b2b4-d1d2362e2a1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10828654-7afb-4e36-b2ed-694e784d8830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0edcc015-3978-451b-bd0a-e2215eb323c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10828654-7afb-4e36-b2ed-694e784d8830",
                    "LayerId": "99c12767-723a-4e25-b485-45b3d1d547f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "99c12767-723a-4e25-b485-45b3d1d547f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d2802cb-d379-4260-938b-38e70efb70d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 0,
    "yorig": 0
}