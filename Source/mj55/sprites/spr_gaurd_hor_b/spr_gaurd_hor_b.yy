{
    "id": "f45ab3a3-7371-4476-934e-a5087e6f02df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_hor_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd666aeb-1e56-44cc-a301-2baf00a3b94f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45ab3a3-7371-4476-934e-a5087e6f02df",
            "compositeImage": {
                "id": "8c541f07-75ea-45c3-bf73-f71b652c85ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd666aeb-1e56-44cc-a301-2baf00a3b94f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5af418a3-8cfd-4298-a1b6-0e185376f3f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd666aeb-1e56-44cc-a301-2baf00a3b94f",
                    "LayerId": "947ae389-8a5a-4072-b95f-f59a38c78749"
                }
            ]
        },
        {
            "id": "2f937594-2167-4fb1-9080-b3b31668d5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f45ab3a3-7371-4476-934e-a5087e6f02df",
            "compositeImage": {
                "id": "4cfb3e50-c303-4b5b-8d71-39a131a8a03f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f937594-2167-4fb1-9080-b3b31668d5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2e49b4-8723-4337-9377-703c2a87942b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f937594-2167-4fb1-9080-b3b31668d5d9",
                    "LayerId": "947ae389-8a5a-4072-b95f-f59a38c78749"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "947ae389-8a5a-4072-b95f-f59a38c78749",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f45ab3a3-7371-4476-934e-a5087e6f02df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}