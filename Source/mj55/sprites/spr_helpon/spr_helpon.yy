{
    "id": "a909b79d-8d08-4381-b76e-e4d1a5063844",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_helpon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 180,
    "bbox_right": 250,
    "bbox_top": 214,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae7600e5-1ead-43be-97f5-e72d4030331f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a909b79d-8d08-4381-b76e-e4d1a5063844",
            "compositeImage": {
                "id": "5478bb94-26d1-43a8-8ecb-bf62e0b6bb1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae7600e5-1ead-43be-97f5-e72d4030331f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99c9e84f-30bd-4823-beb8-a5457ebe6c36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae7600e5-1ead-43be-97f5-e72d4030331f",
                    "LayerId": "ffd6d681-747f-4f3b-bb14-86cf6e5afe6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "ffd6d681-747f-4f3b-bb14-86cf6e5afe6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a909b79d-8d08-4381-b76e-e4d1a5063844",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}