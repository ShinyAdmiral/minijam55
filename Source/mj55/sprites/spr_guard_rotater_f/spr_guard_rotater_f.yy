{
    "id": "81590913-9e61-4c44-b3f3-6f28845f6e7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guard_rotater_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "256459d9-ac79-4554-b742-c6ecb1e620db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81590913-9e61-4c44-b3f3-6f28845f6e7a",
            "compositeImage": {
                "id": "545c754d-520f-4ce6-a280-8949230b848a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "256459d9-ac79-4554-b742-c6ecb1e620db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b649d7c0-9291-4fb7-be66-6114a372ccbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "256459d9-ac79-4554-b742-c6ecb1e620db",
                    "LayerId": "5d9c6079-f2b5-4e71-ad12-ada884ef85c7"
                }
            ]
        },
        {
            "id": "815a1b56-abe6-434b-80d7-ecae5ab4bc7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81590913-9e61-4c44-b3f3-6f28845f6e7a",
            "compositeImage": {
                "id": "425ff643-281c-4f39-a982-8d67bd1bbefb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "815a1b56-abe6-434b-80d7-ecae5ab4bc7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89497b37-e4c0-4c8a-ae45-646a3bae2f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "815a1b56-abe6-434b-80d7-ecae5ab4bc7b",
                    "LayerId": "5d9c6079-f2b5-4e71-ad12-ada884ef85c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5d9c6079-f2b5-4e71-ad12-ada884ef85c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81590913-9e61-4c44-b3f3-6f28845f6e7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}