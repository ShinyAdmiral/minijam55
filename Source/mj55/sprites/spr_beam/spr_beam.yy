{
    "id": "cbc0f9e3-2aac-4500-9569-3cf610c81a27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_beam",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d6fb18f-fd1e-409f-8ac8-90bc5b5ce27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbc0f9e3-2aac-4500-9569-3cf610c81a27",
            "compositeImage": {
                "id": "e7ad2279-7dd1-47fa-955f-930d731bc580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d6fb18f-fd1e-409f-8ac8-90bc5b5ce27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ecb7458-0585-413e-b5ac-f9eeb12b750a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d6fb18f-fd1e-409f-8ac8-90bc5b5ce27f",
                    "LayerId": "1d769976-b24a-40b3-a9ac-4c9687a877dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1d769976-b24a-40b3-a9ac-4c9687a877dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbc0f9e3-2aac-4500-9569-3cf610c81a27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}