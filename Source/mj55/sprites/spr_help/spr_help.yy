{
    "id": "0832025c-266e-4445-83de-7863b0465c08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_help",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 245,
    "bbox_left": 180,
    "bbox_right": 250,
    "bbox_top": 214,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb2ed53c-bd3d-4269-9d3d-b64b68953f4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0832025c-266e-4445-83de-7863b0465c08",
            "compositeImage": {
                "id": "a13364a2-23c9-43ce-92b0-a8106bf93928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2ed53c-bd3d-4269-9d3d-b64b68953f4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beb3f951-5f3d-4f2b-a646-fecdc140c0ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2ed53c-bd3d-4269-9d3d-b64b68953f4e",
                    "LayerId": "fdc92c3f-0148-405d-950a-0427b7908de5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "fdc92c3f-0148-405d-950a-0427b7908de5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0832025c-266e-4445-83de-7863b0465c08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}