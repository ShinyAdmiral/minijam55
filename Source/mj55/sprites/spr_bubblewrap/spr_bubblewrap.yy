{
    "id": "66d249bc-546b-424b-9b99-449379fb9ec0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bubblewrap",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8572cdca-d744-4ed4-b4d2-69397b9c0868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66d249bc-546b-424b-9b99-449379fb9ec0",
            "compositeImage": {
                "id": "bf3626d6-234c-4f3a-a7c0-ad65f6800f42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8572cdca-d744-4ed4-b4d2-69397b9c0868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6a1329-b512-4dbf-9a2a-91e791940365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8572cdca-d744-4ed4-b4d2-69397b9c0868",
                    "LayerId": "69b2b1cc-e4d6-4d15-9c56-8c3808cef561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "69b2b1cc-e4d6-4d15-9c56-8c3808cef561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66d249bc-546b-424b-9b99-449379fb9ec0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}