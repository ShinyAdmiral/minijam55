{
    "id": "d387c3f9-9a4c-49b2-9739-6ee3491cf479",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d71cc827-9202-458a-b810-c3991456a66b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d387c3f9-9a4c-49b2-9739-6ee3491cf479",
            "compositeImage": {
                "id": "8c475fa9-14c3-46e8-9bae-d60ec497d159",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d71cc827-9202-458a-b810-c3991456a66b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a67bec4-6f12-4958-bf4e-b60c3a85a6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d71cc827-9202-458a-b810-c3991456a66b",
                    "LayerId": "c557d747-9007-4b8f-b186-67bbcda39c43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c557d747-9007-4b8f-b186-67bbcda39c43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d387c3f9-9a4c-49b2-9739-6ee3491cf479",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}