{
    "id": "f0c0e1c8-9bcd-4905-bc32-f53422d878c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_stat_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4800f074-38a5-46c8-b69f-5a96c0c214ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0c0e1c8-9bcd-4905-bc32-f53422d878c2",
            "compositeImage": {
                "id": "9c489f79-7a12-47a7-8347-58c5c291e27c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4800f074-38a5-46c8-b69f-5a96c0c214ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1ba2377-fcf9-4fb1-997a-5b2e7d45b3e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4800f074-38a5-46c8-b69f-5a96c0c214ef",
                    "LayerId": "b73f58d8-5613-4117-813e-80a2f0783afd"
                }
            ]
        },
        {
            "id": "6942b899-0981-40d5-908a-8066787e21ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0c0e1c8-9bcd-4905-bc32-f53422d878c2",
            "compositeImage": {
                "id": "65b05e2b-a7e1-494d-9ccd-15f729b296d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6942b899-0981-40d5-908a-8066787e21ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2968aa4b-4cc9-451c-9f77-14ebedb8f3c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6942b899-0981-40d5-908a-8066787e21ff",
                    "LayerId": "b73f58d8-5613-4117-813e-80a2f0783afd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b73f58d8-5613-4117-813e-80a2f0783afd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0c0e1c8-9bcd-4905-bc32-f53422d878c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}