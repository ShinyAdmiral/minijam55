{
    "id": "677f3f13-7799-41b9-8d28-85bd9704a7d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spj_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eec9a87f-c6e9-4441-ac03-62fccda3c20a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "677f3f13-7799-41b9-8d28-85bd9704a7d4",
            "compositeImage": {
                "id": "01830d28-b1a4-40a6-b8a3-cc2c90fef85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eec9a87f-c6e9-4441-ac03-62fccda3c20a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59e4176-86b9-4535-9828-1701138b0996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eec9a87f-c6e9-4441-ac03-62fccda3c20a",
                    "LayerId": "8d1299f0-0102-43ac-9c84-8babaaf58992"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8d1299f0-0102-43ac-9c84-8babaaf58992",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "677f3f13-7799-41b9-8d28-85bd9704a7d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}