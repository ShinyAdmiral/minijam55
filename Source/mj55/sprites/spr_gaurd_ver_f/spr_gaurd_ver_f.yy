{
    "id": "70d566f0-9825-418e-9898-87d55cfcd11b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_ver_f",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "903a57ce-0e1c-4691-9968-ca83f63e8148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70d566f0-9825-418e-9898-87d55cfcd11b",
            "compositeImage": {
                "id": "e06e5d4e-bc8e-4309-886c-ccdfd97148c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "903a57ce-0e1c-4691-9968-ca83f63e8148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66cdd61a-7ad6-4df6-854e-d32b2111d89f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "903a57ce-0e1c-4691-9968-ca83f63e8148",
                    "LayerId": "8c077b6f-1476-4640-8eb7-76d51430bb81"
                }
            ]
        },
        {
            "id": "f16b0650-b9f9-4127-ad80-6ba5c3853e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70d566f0-9825-418e-9898-87d55cfcd11b",
            "compositeImage": {
                "id": "f203379b-9a81-463a-a8dd-8a70227ddad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16b0650-b9f9-4127-ad80-6ba5c3853e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2d872ed-97fa-43e8-b243-83e95c0a6dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16b0650-b9f9-4127-ad80-6ba5c3853e06",
                    "LayerId": "8c077b6f-1476-4640-8eb7-76d51430bb81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8c077b6f-1476-4640-8eb7-76d51430bb81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70d566f0-9825-418e-9898-87d55cfcd11b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}