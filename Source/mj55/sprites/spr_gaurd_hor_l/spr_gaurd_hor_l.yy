{
    "id": "9cdbdf71-ed94-48c3-93b4-8278acf4c96e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gaurd_hor_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbce2ac4-89ff-4eb4-a2c2-0bf3552e9b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdbdf71-ed94-48c3-93b4-8278acf4c96e",
            "compositeImage": {
                "id": "f1e23e89-2456-4504-a85b-a7059273f789",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbce2ac4-89ff-4eb4-a2c2-0bf3552e9b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c73bd0ba-fde0-49bf-8fcd-e91a8c023b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbce2ac4-89ff-4eb4-a2c2-0bf3552e9b6c",
                    "LayerId": "a6dcf71a-73ea-4e55-ab28-5bf140e5eddf"
                }
            ]
        },
        {
            "id": "4b2b9513-079b-4f57-9528-184e5e45d039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdbdf71-ed94-48c3-93b4-8278acf4c96e",
            "compositeImage": {
                "id": "74a80f5e-50d5-4bd5-8f6b-ee0e41d69883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b2b9513-079b-4f57-9528-184e5e45d039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbfe3521-3749-4807-87bb-67de01df96b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2b9513-079b-4f57-9528-184e5e45d039",
                    "LayerId": "a6dcf71a-73ea-4e55-ab28-5bf140e5eddf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a6dcf71a-73ea-4e55-ab28-5bf140e5eddf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cdbdf71-ed94-48c3-93b4-8278acf4c96e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}