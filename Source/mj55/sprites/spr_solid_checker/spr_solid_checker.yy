{
    "id": "964a0922-964c-4bab-abd6-e44e104eb88a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_solid_checker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c5ed025-b254-4e28-a763-540916753494",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "964a0922-964c-4bab-abd6-e44e104eb88a",
            "compositeImage": {
                "id": "fefefd8c-400f-4e85-9444-43c7365a2a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c5ed025-b254-4e28-a763-540916753494",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b927f6de-4b72-4438-b536-e486452e3461",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c5ed025-b254-4e28-a763-540916753494",
                    "LayerId": "33fd9da1-02fd-4e1c-a9e0-88fc43734dcb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33fd9da1-02fd-4e1c-a9e0-88fc43734dcb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "964a0922-964c-4bab-abd6-e44e104eb88a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}