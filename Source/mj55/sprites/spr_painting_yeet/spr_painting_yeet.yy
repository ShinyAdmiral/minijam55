{
    "id": "47e1a408-f667-43b5-8079-29bb5c959746",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_painting_yeet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5dc0f99-5af4-4452-bc65-8e9bca90d780",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47e1a408-f667-43b5-8079-29bb5c959746",
            "compositeImage": {
                "id": "2e9c43bd-6026-4948-8594-b29cf6e7fa73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5dc0f99-5af4-4452-bc65-8e9bca90d780",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638fa21c-391f-4889-b250-d68e1ac89df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5dc0f99-5af4-4452-bc65-8e9bca90d780",
                    "LayerId": "bbc1dd28-8719-4269-b1c7-c2e8ddc72ba6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bbc1dd28-8719-4269-b1c7-c2e8ddc72ba6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47e1a408-f667-43b5-8079-29bb5c959746",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}