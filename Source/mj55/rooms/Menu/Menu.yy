
{
    "name": "Menu",
    "id": "1a006449-8660-453a-95b2-7740a1a55a3c",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "eb7f1ec0-952b-4794-a9a3-09f54be10b79",
        "7868268a-82ee-4717-9a94-e08a6ca01788",
        "ebac44fc-2fd7-4c60-a71a-e251ecff8008",
        "4c912fd4-c31a-47ff-87c7-a6a4241a69b8",
        "0aaa2127-ff85-4b6e-94bf-eb3a6401ab27",
        "683b7b95-8a6d-44c8-9372-1e5f08e87274",
        "200c6873-575b-4b61-a801-b4ffe32d8c7e"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances_1",
            "id": "96203202-a93a-4dbf-a8fc-7adc4f434fc9",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_72DE86FB","id": "eb7f1ec0-952b-4794-a9a3-09f54be10b79","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_72DE86FB","objId": "7f43fcf2-4430-4486-b62a-9fe678bcd529","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -16,"y": -16}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRAssetLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Assets_1",
            "id": "4d91773c-798d-4f5c-b526-19586c72558f",
            "assets": [

            ],
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRAssetLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances_2",
            "id": "4d25b8be-0748-4529-b2cd-2ba1245c1ff8",
            "depth": 200,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_38DD8AAA","id": "7868268a-82ee-4717-9a94-e08a6ca01788","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_38DD8AAA","objId": "6c059270-2efd-43c1-ab15-1a0f3e12454b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -12,"y": 20},
{"name": "inst_38EB48FD","id": "ebac44fc-2fd7-4c60-a71a-e251ecff8008","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_38EB48FD","objId": "c9434a80-4aae-40fc-9264-6edde0ba205e","properties": [{"id": "1adb511f-3d66-4cb8-bcaa-e24b3e66a9b0","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "c1ed3925-8c7a-4f31-9f8b-716b9cba5f82","mvc": "1.0","value": "\"Spot Light\""},{"id": "d01800b2-452f-43c6-8448-a075b406e14d","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "92018cda-b3ec-4acc-92ea-6fc9ba6e7683","mvc": "1.0","value": "225"},{"id": "564f8a44-5fc5-4ffc-8af5-bf1b847fae8b","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "7e228cd7-f34a-4384-865f-9f3ce52caf60","mvc": "1.0","value": "1"},{"id": "d1e28a84-c2b2-4d96-be9b-73c09e8bf6e6","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "a9005213-eaa0-40c5-8bda-7f686bb6b127","mvc": "1.0","value": "13.9"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 352,"y": 96},
{"name": "inst_3207D26A","id": "4c912fd4-c31a-47ff-87c7-a6a4241a69b8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3207D26A","objId": "c9434a80-4aae-40fc-9264-6edde0ba205e","properties": [{"id": "7900d1d9-ec31-4bc2-b52f-81b7dbf415e0","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "92018cda-b3ec-4acc-92ea-6fc9ba6e7683","mvc": "1.0","value": "315"},{"id": "141f3204-5526-4ded-ac8f-6df65769b23f","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "c1ed3925-8c7a-4f31-9f8b-716b9cba5f82","mvc": "1.0","value": "\"Spot Light\""},{"id": "c7ed4806-3d66-4979-8a21-69d78fcf29ce","modelName": "GMOverriddenProperty","objectId": "c9434a80-4aae-40fc-9264-6edde0ba205e","propertyId": "a9005213-eaa0-40c5-8bda-7f686bb6b127","mvc": "1.0","value": "13.9"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 128,"y": 96},
{"name": "inst_7710D3D3","id": "0aaa2127-ff85-4b6e-94bf-eb3a6401ab27","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7710D3D3","objId": "8f9da833-0a93-4bd7-82fe-c43d6c494b8b","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 0,"y": 0},
{"name": "inst_5868027B","id": "683b7b95-8a6d-44c8-9372-1e5f08e87274","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5868027B","objId": "8a0e568e-dc59-4230-80c8-0318c5f4d613","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 480,"y": 192},
{"name": "inst_299DA4A5","id": "200c6873-575b-4b61-a801-b4ffe32d8c7e","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_299DA4A5","objId": "1978df4f-061a-43b9-a688-fcca1cca8219","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -32,"y": 128}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRTileLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Tiles_1",
            "id": "f4357824-5113-499c-9f4e-f8f4d839cdd3",
            "depth": 300,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRTileLayer",
            "prev_tileheight": 32,
            "prev_tilewidth": 32,
            "mvc": "1.0",
            "tiles": {
                "SerialiseData": null,
                "SerialiseHeight": 9,
                "SerialiseWidth": 15,
                "TileSerialiseData": [
                    3,2,3,2,3,2,3,2,3,2,3,2,3,2,3,
                    13,12,13,12,13,12,13,12,13,12,13,12,13,12,13,
                    3,2,3,2,3,2,3,2,3,2,3,2,3,2,3,
                    13,12,13,12,13,12,13,12,13,12,13,12,13,12,13,
                    3,2,3,2,3,2,3,2,3,2,3,2,3,2,3,
                    13,12,13,12,13,12,13,12,13,12,13,12,13,12,13,
                    3,2,3,2,3,2,3,2,3,2,3,2,3,2,3,
                    13,12,13,12,13,12,13,12,13,12,13,12,13,12,13,
                    3,2,3,2,3,2,3,2,3,2,3,2,3,2,3
                ]
            },
            "tilesetId": "fd3b3fe1-0605-41dc-bf62-499f80bb0f39",
            "userdefined_depth": false,
            "visible": true,
            "x": 0,
            "y": 0
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "cecd92e2-1b99-4780-95ec-abeb58e30ff3",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4278190080 },
            "depth": 400,
            "grid_x": 4,
            "grid_y": 4,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "00000000-0000-0000-0000-000000000000",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "6e973aa3-6a6b-4983-882b-efdf867a049a",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "531f2692-766e-4911-b3ed-386214d5469d",
        "Height": 270,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 480
    },
    "mvc": "1.0",
    "views": [
{"id": "d1556dc9-6966-48cc-94ba-e1a1f737b0cb","hborder": 32,"hport": 270,"hspeed": -1,"hview": 270,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 480,"wview": 480,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "045a55ac-6bac-48bd-a89a-8ec44033a9fa","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "4daf3df5-cffb-4b15-b8ff-71d0936905e5","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e5471abc-daec-42bc-8d93-2d05ef552d5e","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d6027304-504b-4ff3-9e3e-30f1e1e62a9b","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0bf75e53-b7ea-4614-a1ac-73a187a30432","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "10537951-4384-4e0e-9266-a9122578759c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "725e85f1-6383-476b-8340-8a4b95d81565","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "49f0e27c-8c54-4af1-87df-f5aa5e55f7dc",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}