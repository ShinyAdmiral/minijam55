hor_speed = 0;
ver_speed = 0;

mp_step.x = argument0;
mp_step.y = argument1;
Move_Direction(mp_step);
					
var found = false;
						
if (Check_Wall(mp_step)){
	found = true;
}
else{
	found = Check_For_Previous(mp_step, pos_mem);
}
						
if (found){
	if (dis_to_object > argument8  && dis_to_object < argument8+180){
		mp_step.x = argument2;
		mp_step.y = argument3;
		Move_Direction(mp_step);
		found = false;
						
		if (Check_Wall(mp_step)){
			found = true;
		}
		else{
			found = Check_For_Previous(mp_step, pos_mem);
		}
	}
}	

if found{
	mp_step.x = argument4;
	mp_step.y = argument5;
	Move_Direction(mp_step);
	found = false;
						
	if (Check_Wall(mp_step)){
		found = true;
	}
	else{
		found = Check_For_Previous(mp_step, pos_mem);
	}
}
						
if (found){
	mp_step.x = argument6;
	mp_step.y = argument7;
	Move_Direction(mp_step);
	found = false;
						
	if (Check_Wall(mp_step)){
		found = true;
	}
	else{
		found = Check_For_Previous(mp_step, pos_mem);
	}
}

Move(found);