#macro TILE_SIZE 32
#macro SONG_DECIMAL 100
#macro ENEMY_MEMORY 20
#macro MAX_NOTICE_Y 150
#macro MAX_NOTICE_X 150
#macro MAX_DISTANCE 240

enum enemy_state {
	patrol,
	suspicious,
	returning
};