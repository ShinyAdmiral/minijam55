center_x = x + TILE_SIZE/2;
center_y = y + TILE_SIZE/2;

switch (dir){
	case(0):
		with (light){
			x = enemy_id.x + TILE_SIZE;
			y = enemy_id.center_y;
			light[|eLight.X] = x;
			light[|eLight.Y] = y;
			light[| eLight.Direction] = 0;
		}
	
		sight_a.x = x + TILE_SIZE;
		sight_a.y = y;
		with (sight_a){
			if (place_meeting(x,y,obj_solid)){
				enemy_id.sight_b.sight = true;
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_b.x = x + (TILE_SIZE * 2);
		sight_b.y = y;
		with (sight_b){
			if (place_meeting(x,y,obj_solid) || sight){
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_c.x = x + (TILE_SIZE * 3);
		sight_c.y = y;
		with (sight_c){
			if (place_meeting(x,y,obj_solid) || sight){
				instance_deactivate_object(self);
			}
		}
		
		sprite_index = r;
		
		break;
	
	case(90):
		with (light){
			x = enemy_id.center_x;
			y = enemy_id.y;
			light[|eLight.X] = x;
			light[|eLight.Y] = y;
			light[| eLight.Direction] = 90;
		}
		
		sight_a.x = x;
		sight_a.y = y - TILE_SIZE;
		with (sight_a){
			if (place_meeting(x,y,obj_solid)){
				enemy_id.sight_b.sight = true;
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_b.x = x;
		sight_b.y = y - (TILE_SIZE * 2);
		with (sight_b){
			if (place_meeting(x,y,obj_solid) || sight){
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_c.x = x;
		sight_c.y = y - (TILE_SIZE * 3);
		with (sight_c){
			if (place_meeting(x,y,obj_solid) || sight){
				instance_deactivate_object(self);
			}
		}
		
		sprite_index = b;
		break;
		
	case(180):
		with (light){
			x = enemy_id.x;
			y = enemy_id.center_y;
			light[|eLight.X] = x;
			light[|eLight.Y] = y;
			light[| eLight.Direction] = 180;
		}
	
		sight_a.x = x - TILE_SIZE;
		sight_a.y = y;
		with (sight_a){
			if (place_meeting(x,y,obj_solid)){
				enemy_id.sight_b.sight = true;
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_b.x = x - (TILE_SIZE * 2);
		sight_b.y = y;
		with (sight_b){
			if (place_meeting(x,y,obj_solid) || sight){
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_c.x = x - (TILE_SIZE * 3);
		sight_c.y = y;
		with (sight_c){
			if (place_meeting(x,y,obj_solid) || sight){
				instance_deactivate_object(self);
			}
		}
		sprite_index = l;
		break;
		
	case(270):
		with (light){
			x = enemy_id.center_x;
			y = enemy_id.y + TILE_SIZE;
			light[|eLight.X] = x;
			light[|eLight.Y] = y;
			light[| eLight.Direction] = 270;
		}
	
		sight_a.x = x;
		sight_a.y = y + TILE_SIZE;
		with (sight_a){
			if (place_meeting(x,y,obj_solid)){
				enemy_id.sight_b.sight = true;
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_b.x = x;
		sight_b.y = y + (TILE_SIZE * 2);
		with (sight_b){
			if (place_meeting(x,y,obj_solid) || sight){
				enemy_id.sight_c.sight = true;
				instance_deactivate_object(self);
			}
		}
		
		sight_c.x = x;
		sight_c.y = y + (TILE_SIZE * 3);
		with (sight_c){
			if (place_meeting(x,y,obj_solid) || sight){
				instance_deactivate_object(self);
			}
		}
		sprite_index = f;
		break;
}