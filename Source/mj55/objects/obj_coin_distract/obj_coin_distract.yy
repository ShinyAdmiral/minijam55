{
    "id": "382c0783-31bc-4669-9a79-f555670716cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin_distract",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd5d0840-e8eb-4bdc-ac3f-0b5019fbbb22",
    "visible": true
}