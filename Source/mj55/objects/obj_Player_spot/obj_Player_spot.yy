{
    "id": "7e3cc439-6902-4a08-a29e-878cf03a329e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player_spot",
    "eventList": [
        {
            "id": "ad9b37a6-570c-42cd-b888-91db6f915fe1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7e3cc439-6902-4a08-a29e-878cf03a329e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "550d318c-22df-4533-8d24-c23684e2dfa4",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "a89637b7-5f88-4100-a038-c4d3e5c67480",
            "value": "17.2"
        }
    ],
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}