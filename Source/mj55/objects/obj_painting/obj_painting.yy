{
    "id": "ae626ac6-a222-41db-a871-80a28481637d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_painting",
    "eventList": [
        {
            "id": "518f4b0b-95cc-4185-b690-0a0414c4f39b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ae626ac6-a222-41db-a871-80a28481637d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "5c95a957-debb-4174-befa-fcea930ac136",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "12977b57-3c14-4684-a860-15eef892e37e",
            "value": "\"Spot Light\""
        },
        {
            "id": "2c6a4664-08dd-4bf4-8cde-06d3cb4fdd67",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "5686c0db-78e4-4fd7-89e5-0ed522384dd8",
            "value": "60"
        },
        {
            "id": "004a1f0c-2b1b-4cea-9f9b-19e7290d42e5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "b70554c1-2210-49d5-84db-fb0c2df6cbc1",
            "value": "270"
        },
        {
            "id": "ba3816c6-fed7-4333-8296-a7bf3ee08764",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "1a4e0384-2c2a-48c1-86ff-1579eb84c5fa",
            "value": "91"
        },
        {
            "id": "8614d3c3-1d27-4c7c-b8ab-420608b887f8",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "a89637b7-5f88-4100-a038-c4d3e5c67480",
            "value": "2.8"
        }
    ],
    "parentObjectId": "58ddfd5b-d82b-4ac4-9413-c9043ccca2da",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "90d4a9de-82cf-4a5a-a348-cd49522a6c4c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "spr_painting_ricardo",
            "varName": "sprite",
            "varType": 5
        }
    ],
    "solid": false,
    "spriteId": "2be63fad-5fba-4623-98e9-b5b90f6a609e",
    "visible": true
}