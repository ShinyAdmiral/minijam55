{
    "id": "42f50fea-4463-4bb9-8d97-56d5d973fd80",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_suprise",
    "eventList": [
        {
            "id": "fb0b44d8-8dc6-40d3-a7bc-dae5aa0e7538",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42f50fea-4463-4bb9-8d97-56d5d973fd80"
        },
        {
            "id": "5e401fd8-8f4f-4827-b188-726d9c1e0e45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "42f50fea-4463-4bb9-8d97-56d5d973fd80"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72dbe503-e617-4e43-b014-0bb6bf4aebd1",
    "visible": true
}