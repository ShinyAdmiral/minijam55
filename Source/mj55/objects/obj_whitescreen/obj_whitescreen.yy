{
    "id": "f863cb35-fdcd-4aa4-9248-391df324192f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_whitescreen",
    "eventList": [
        {
            "id": "2cc7c305-775f-4202-b203-dd5599b61d6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f863cb35-fdcd-4aa4-9248-391df324192f"
        },
        {
            "id": "8f378b41-d01a-4344-8544-f3d801824604",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f863cb35-fdcd-4aa4-9248-391df324192f"
        },
        {
            "id": "437273aa-b15b-44c5-b687-0f136fba6f53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "f863cb35-fdcd-4aa4-9248-391df324192f"
        },
        {
            "id": "0bb145fb-44c3-4eac-85f9-d0a40874f0d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f863cb35-fdcd-4aa4-9248-391df324192f"
        },
        {
            "id": "c08cc540-cbb5-4933-965a-ec35742d4378",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f863cb35-fdcd-4aa4-9248-391df324192f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd7a500f-3bde-41f3-90f6-c79f817f3525",
    "visible": true
}