/// @description Insert description here
instance_destroy(oMusic);
instance_destroy(obj_level_one_music);
instance_destroy(obj_player);
instance_destroy(obj_guard);
instance_destroy(obj_laser_emmitter);
instance_destroy(obj_visualizer);
instance_destroy(obj_visualizer_image);
instance_destroy(obj_UI);
instance_destroy(obj_camera);
instance_destroy(obj_rave_light);
audio_stop_all();
alarm[0] = 2;