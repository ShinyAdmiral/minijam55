{
    "id": "a83e4180-53c0-4a54-adc4-708dca5e9eab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laser_emmitter",
    "eventList": [
        {
            "id": "00b23509-4690-43ca-9822-8f89cc693a8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a83e4180-53c0-4a54-adc4-708dca5e9eab"
        },
        {
            "id": "ad7232b7-fc59-4af2-916c-d49fa5f9d39a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a83e4180-53c0-4a54-adc4-708dca5e9eab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0a9ab106-41b2-48ea-aff7-d7b6db8ccd3c",
    "visible": true
}