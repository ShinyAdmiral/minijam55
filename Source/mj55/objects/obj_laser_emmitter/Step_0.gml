/// @description Insert description here
if (obj_player.operate){
	if (obj_music.move && cool){
		tick++;
		cool = false;
	}
	else if (!obj_music.move){
		cool = true;
	}

	if (tick = max_tick/2){
		if instance_exists(obj_laser){
			if (obj_laser.x > obj_camera.camera_x && obj_laser.x < obj_camera.camera_x + obj_camera.camera_width){
				if (obj_laser.y > obj_camera.camera_y && obj_laser.y < obj_camera.camera_y + obj_camera.camera_height){
					audio_stop_sound(laser_on);
					ran = irandom_range(.8, 1.2);
					audio_sound_pitch(laser_off, ran);
					audio_play_sound(laser_off, 20, false);
				}
			}
			instance_destroy(obj_laser);
			sprite_index = spr_laser_left;
			obj_laser_emmitter.sprite_index = spr_laser_left;
			obj_laser_stopper.sprite_index = spr_laser_right;
		}
	}

	if (tick >= max_tick){
		instance_create_layer(x - TILE_SIZE, y,"Lasers", obj_laser);
		if (obj_laser.x > obj_camera.camera_x && obj_laser.x < obj_camera.camera_x + obj_camera.camera_width){
			if (obj_laser.y > obj_camera.camera_y && obj_laser.y < obj_camera.camera_y + obj_camera.camera_height){
				audio_stop_sound(laser_off);
				ran = irandom_range(.8, 1.2);
				audio_sound_pitch(laser_on, ran);
				audio_play_sound(laser_on, 20, false);
			}
		}
		sprite_index = spr_laser_left_on;
		obj_laser_emmitter.sprite_index = spr_laser_left_on;
		obj_laser_stopper.sprite_index = spr_laser_right_on;
		tick = 0;
	}

	if (instance_exists(obj_laser)){
		if (obj_laser.x > obj_camera.camera_x && obj_laser.x < obj_camera.camera_x + obj_camera.camera_width){
			if (obj_laser.y > obj_camera.camera_y && obj_laser.y < obj_camera.camera_y + obj_camera.camera_height){
				oMusic.lasers = true;
			}
		}
	}
	else{
		oMusic.lasers = false;
	}
}