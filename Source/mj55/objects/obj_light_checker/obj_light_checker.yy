{
    "id": "d87109a7-3cd1-4547-ba55-c3936f51c157",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light_checker",
    "eventList": [
        {
            "id": "7d5d0e3d-4973-4216-b8b0-b102e86b94d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d87109a7-3cd1-4547-ba55-c3936f51c157"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b8e08f2-28f5-4333-b979-8dfd12ca8eb5",
    "visible": true
}