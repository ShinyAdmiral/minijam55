/// @description Insert description here
event_inherited();

move_dir = choose(-1,1);

dir = 0;

if (move_dir < 0){
	dir = 180;
}

sight_a = instance_create_layer(x,y,"Sight", obj_light_checker);
sight_a.enemy_id = id;
sight_b = instance_create_layer(x,y,"Sight", obj_light_checker);
sight_b.enemy_id = id;
sight_c = instance_create_layer(x,y,"Sight", obj_light_checker);
sight_c.enemy_id = id;

light = instance_create_layer(x,y, "Light_Renderer", obj_Guard_light);
light.enemy_id = id;

mp_step = instance_create_layer(x,y,"Management", obj_solid_checker_enemy);
mp_step.enemy_id = id;

var i = 0;
repeat(ENEMY_MEMORY){
	pos_mem[i, 0] = -1;
	pos_mem[i, 1] = -1;
	i++
}
index = 0;

with (sight_b){
	sight = false;
}

with (sight_c){
	sight = false;
}

cool = false;

move_speed = 4;
horizontal_movement = 0;
vertical_movement = 0;

return_object = id;

show = true;
state = enemy_state.patrol;

//sprites
f = spr_gaurd_hor_f;
b = spr_gaurd_hor_b;
r = spr_gaurd_hor_r;
l = spr_gaurd_hor_l;
