{
    "id": "f170f2f6-8431-4cd2-9453-1386cc93bfa6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guard_hor",
    "eventList": [
        {
            "id": "6ac4313e-a15d-477d-a661-7f6299abf0fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f170f2f6-8431-4cd2-9453-1386cc93bfa6"
        },
        {
            "id": "95d5a3b9-aad9-465d-b1ec-848092ce86a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f170f2f6-8431-4cd2-9453-1386cc93bfa6"
        },
        {
            "id": "fc87c2bd-95f9-445a-9fea-2d6d46c5e2f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f170f2f6-8431-4cd2-9453-1386cc93bfa6"
        },
        {
            "id": "d7bc8c3c-3ab7-4770-9610-482fcd55d940",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f170f2f6-8431-4cd2-9453-1386cc93bfa6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36895d98-fb68-4d70-8887-76923fc804d2",
    "visible": true
}