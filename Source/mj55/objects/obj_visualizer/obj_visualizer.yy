{
    "id": "5a5ab0fd-fbcf-4b28-862e-5fef73b8383a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_visualizer",
    "eventList": [
        {
            "id": "fd56c944-21c0-493c-a022-615541041e78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a5ab0fd-fbcf-4b28-862e-5fef73b8383a"
        },
        {
            "id": "e923c8bc-7e0c-4033-af5b-add625a55c72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5a5ab0fd-fbcf-4b28-862e-5fef73b8383a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "74cd92cf-d251-4aef-94b8-ceb0d596e5d0",
    "visible": true
}