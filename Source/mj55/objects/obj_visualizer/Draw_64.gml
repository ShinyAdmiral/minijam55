/// @description Insert description here
if (obj_player.operate){
	draw_sprite_ext(spr_ui_box_1, 0, 240, 240, scale * xs, scale, 0, c_white, alpha)

	if (shrink){
		scale += ratio;
		if (cool){
			var a = instance_create_layer(number, y, "Instances_1", obj_visualizer_image);
			a.move = number;
			number *= -1;
			cool = false;
		}
	}
	else if (!shrink){
		if (scale <= 1 && !cool){
			scale = 1;
			xs *= -1;
			cool = true;
		}
		else if (scale > 1){
			scale -= ratio*3;
		}
	}
}