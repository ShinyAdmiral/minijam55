{
    "id": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_distraction",
    "eventList": [
        {
            "id": "0fa7e0da-6802-4f5d-b104-25de80834d1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "69a03dcc-76a2-41cb-bc10-f3d2325f6908"
        },
        {
            "id": "0aed6c41-f500-4705-9d03-777776d748e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b11a3e79-f1de-4c7e-8402-a89a66284f76",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "69a03dcc-76a2-41cb-bc10-f3d2325f6908"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e8015ca-779b-4732-a824-668beafa6c80",
    "visible": true
}