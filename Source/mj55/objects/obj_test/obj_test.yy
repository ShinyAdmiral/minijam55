{
    "id": "07837c64-9301-447c-8032-8b032750714b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_test",
    "eventList": [
        {
            "id": "7dc7bfd7-f286-441d-850a-e63ea7c5dbb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07837c64-9301-447c-8032-8b032750714b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "8ddba1ea-8a29-42bd-bfbe-e66df6fc36fe",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "12977b57-3c14-4684-a860-15eef892e37e",
            "value": "\"Spot Light\""
        },
        {
            "id": "6c2d079e-e1d0-491d-8513-38f7d79fa2ee",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "acc21797-5541-499d-a7a7-ca1316572c37",
            "value": "$FF00FFFF"
        },
        {
            "id": "fbb45893-f149-407c-94c2-93bfd41d81f6",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "5686c0db-78e4-4fd7-89e5-0ed522384dd8",
            "value": "20"
        },
        {
            "id": "c2e169d2-d2ba-41f6-aad8-b0f85de35240",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "1a4e0384-2c2a-48c1-86ff-1579eb84c5fa",
            "value": "150"
        },
        {
            "id": "5043479e-0eab-420a-b517-1c61d3b53e95",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "a89637b7-5f88-4100-a038-c4d3e5c67480",
            "value": "3.9"
        }
    ],
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}