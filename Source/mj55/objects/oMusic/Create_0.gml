/// @description Insert description here
// You can write your code in this editor
bgm = audio_play_sound(adaptive_music, 10, false);
bgm_laser = audio_play_sound(guard_music, 10, false);
bgm_guard = audio_play_sound(laser_music, 10, false);
audio_sound_gain(adaptive_music, 0.5, 0);
audio_sound_gain(guard_music, 0, 0);
audio_sound_gain(laser_music, 0, 0);

intro_length = 12.308;
loop_length = 24.616;
intensity = 1;
transitioning = false;
total_length = intro_length+loop_length;
guards = false;
lasers = false;