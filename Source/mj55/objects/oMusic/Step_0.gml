/// @description Insert description here
// You can write your code in this editor

var currentPos = audio_sound_get_track_position(bgm);

if (obj_awareness.awareness > 0){
	guards = true;
}

else if (obj_awareness.awareness <= 0){
	guards = false;
	obj_awareness.awareness = 0;
}

//if(keyboard_check_pressed(vk_up))
//{
//	intensity++;
//}

//if(keyboard_check_pressed(vk_left))
//{
//	guards = true;
//}

//if(keyboard_check_pressed(vk_right))
//{
//	guards = false;
//}

//if(keyboard_check_pressed(vk_shift))
//{
//	lasers = false;
//}

//if(keyboard_check_pressed(vk_space))
//{
//	lasers = true;
//}




if(transitioning == true)
{
	if(intensity == 2)
	{
		if(currentPos >= 24.616)	
		{
			intro_length = 24.616;
			total_length = intro_length + loop_length;
			transitioning = false;
		}
	}
	
	
	if(intensity == 3)
	{
		if(currentPos >= 36.924)	
		{
			intro_length = 36.924;
			total_length = intro_length + loop_length;
			transitioning = false;
		}
	}
	
	if(intensity == 3)
	{
		if(currentPos >= 61.54)	
		{
			intro_length = 61.54;
			total_length = intro_length + loop_length;
			transitioning = false;
		}
	}
	
}

if(intensity == 2)
{
	loop_length = 36.924;
	total_length = 24.616 + loop_length;
	transitioning = true;
}

if(intensity == 3)
{
	loop_length = 73.848;
	total_length = 36.924 + loop_length;
	transitioning = true;
}

if(intensity >= 4)
{
	loop_length = 73.848;
	total_length = 61.54 + loop_length;
	transitioning = true;
}

if(currentPos > total_length)
{
	audio_sound_set_track_position(bgm, (currentPos-loop_length));
	audio_sound_set_track_position(bgm_guard, (currentPos-loop_length));
	audio_sound_set_track_position(bgm_laser, (currentPos-loop_length));
}

if(guards == true)
{
	audio_sound_gain(guard_music, 1, 4000);
}

if(guards == false)
{
	audio_sound_gain(guard_music, 0, 4000);
}



if(lasers == true)
{
	audio_sound_gain(laser_music, 1, 4000);
}

if(lasers == false)
{
	audio_sound_gain(laser_music, 0, 4000);
}