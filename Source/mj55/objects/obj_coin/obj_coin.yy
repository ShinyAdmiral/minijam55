{
    "id": "2070d314-3db6-4d2a-b412-0c658b9e29a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coin",
    "eventList": [
        {
            "id": "536b5c34-d36f-4224-bc9c-f4b4f0ed62d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a0500300-af83-489e-a58b-9c1e1152179c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2070d314-3db6-4d2a-b412-0c658b9e29a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd5d0840-e8eb-4bdc-ac3f-0b5019fbbb22",
    "visible": true
}