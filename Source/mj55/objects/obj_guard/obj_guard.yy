{
    "id": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guard",
    "eventList": [
        {
            "id": "c48299a1-b716-41a9-975e-ed75cf0e95c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a0e568e-dc59-4230-80c8-0318c5f4d613"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "659c1ed8-6be9-4c9b-9a9a-5d0762ca1f13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5659f7e7-1a90-40f6-a90e-06ec46b6bb44",
    "visible": true
}