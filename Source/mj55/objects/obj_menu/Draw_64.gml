/// @description Insert description here

switch (state){
	case (MENU_STATE.main):
		if (keyboard_check_pressed(ord("A")) || keyboard_check_pressed(vk_left) || keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down)) {
			menu_index --;
			audio_play_sound(player_step, 10, false);
		}
		else if (keyboard_check_pressed(ord("D")) || keyboard_check_pressed(vk_right) || keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up)){
			menu_index ++;
			audio_play_sound(player_step, 10, false);
		}
		
		if (menu_index > 2)
			menu_index = 0;
		if (menu_index < 0)
			menu_index = 2;
		
		draw_sprite(spr_title, 0, 0, 0);
		
		switch(menu_index){
			case(0):
				draw_sprite(spr_credits, 0, 0, 0);
				draw_sprite(spr_helpon, 0, 0 , 0);
				draw_sprite(spr_start, 0, 0 , 0);
				draw_sprite(spr_statue, 0, 0, 0);
				if (keyboard_check_pressed(vk_space)){
					audio_play_sound(grab_item, 10, false);
					state = MENU_STATE.help;
				}
				break;
				
			case(1):
				draw_sprite(spr_credits, 0, 0, 0);
				draw_sprite(spr_help, 0, 0 , 0);
				draw_sprite(spr_starton, 0, 0 , 0);
				draw_sprite(spr_statue, 0, 0, 0);
				if (keyboard_check_pressed(vk_space)){
					audio_stop_all();
					audio_play_sound(grab_item, 10, false);
					room_goto(room01);
				}
				break;
			
			case(2):
				draw_sprite(spr_creditson, 0, 0, 0);
				draw_sprite(spr_help, 0, 0 , 0);
				draw_sprite(spr_start, 0, 0 , 0);
				draw_sprite(spr_statue, 0, 0, 0);
				if (keyboard_check_pressed(vk_space)){
					audio_play_sound(grab_item, 10, false);
					state = MENU_STATE.credits;
				}
				break;
		}
		break;
	
	case (MENU_STATE.credits):
		draw_set_font(font_Lable);
		draw_set_color(c_white);
		draw_text(5, 1, "Credits:");

		draw_set_font(font_painting);
		draw_set_color(c_white);
		draw_text(5, 25, "Andrew Hunt - Programming");
		draw_text(5, 45, "Bryn Hooper - Artist");
		draw_text(5, 65, "Michael St. Louis - Sound, Programming");

		draw_set_font(font_Lable);
		draw_set_color(c_white);
		draw_text(5, 95, "Special Thanks To...");

		draw_set_font(font_painting);
		draw_set_color(c_white);
		draw_text(5, 119, "thecode.cafe - Open Source Lighting System");
		draw_text(5, 139, "1histori (from freesound.org) - coin dropping sfx");

		draw_text(5, 245, "(Press Space to go Back)");
		
		if (keyboard_check_pressed(vk_space)){
			audio_play_sound(grab_item, 10, false);
			state = MENU_STATE.main;
		}
		break;
		
	case (MENU_STATE.help):
		draw_set_font(font_Lable);
		draw_set_color(c_white);
		draw_text(5, 1, "Controls:");

		draw_set_font(font_painting);
		draw_set_color(c_white);
		draw_text(5, 25, "WASD - (Move and Grab Item)");
		draw_text(5, 45, "Space - (Drop Coins for Distraction)");
		draw_text(5, 65, "Escape - (Quit Game)");
		draw_text(5, 85, "All actions must be done to the beat of the music...");

		draw_set_font(font_Lable);
		draw_set_color(c_white);
		draw_text(5, 115, "Goal:");

		draw_set_font(font_painting);
		draw_set_color(c_white);
		draw_text(5, 139, "Grab all the Museum  paintings and statues and");
		draw_text(5, 159, "escape undetected.");
		draw_text(5, 179, "Use coins to distract guards and Maneuver the Museum.");

		draw_text(5, 245, "(Press Space to go Back)");
		
		if (keyboard_check_pressed(vk_space)){
			audio_play_sound(grab_item, 10, false);
			state = MENU_STATE.main;
		}
		break;
}
