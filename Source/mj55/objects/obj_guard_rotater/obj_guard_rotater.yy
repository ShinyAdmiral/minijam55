{
    "id": "d8995c13-24f8-4e37-ad6b-4b8b5da504f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guard_rotater",
    "eventList": [
        {
            "id": "bcef33ee-c130-4d73-8c81-640854999269",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d8995c13-24f8-4e37-ad6b-4b8b5da504f7"
        },
        {
            "id": "a23f1bde-0128-4cce-b3ef-68e9700a28ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8995c13-24f8-4e37-ad6b-4b8b5da504f7"
        },
        {
            "id": "194b6fbc-f841-4d36-8b95-d0bd07299b42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8995c13-24f8-4e37-ad6b-4b8b5da504f7"
        },
        {
            "id": "8a5202cb-b8f8-4395-8ba4-76c712bfd801",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d8995c13-24f8-4e37-ad6b-4b8b5da504f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "81590913-9e61-4c44-b3f3-6f28845f6e7a",
    "visible": true
}