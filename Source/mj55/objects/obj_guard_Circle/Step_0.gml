/// @description Insert description here
if (obj_player.operate){
	polygon = polygon_from_instance(id);

	show_debug_message(pos)

	switch (state){
		case (enemy_state.patrol):
			#region Patrol
			if (obj_music.move && !cool){
			
				switch(pos){
					case(0):
						if (move_dir > 0){
							mp_step.x = x + TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 0;
							hspeed = move_speed;
							pos++;
						}
						else{
							mp_step.x = x - TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 180;
							hspeed = -move_speed;
							pos--;
						}
						break;
					
					case(1):
						if (move_dir > 0){
							mp_step.x = x;
							mp_step.y = y - TILE_SIZE * move_dir;
							dir = 90;
							vspeed = -move_speed;
							pos++;
						}
						else{
							mp_step.x = x - TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 180;
							hspeed = -move_speed;
							pos--;
						}
						break;
					
					case(2):
						if (move_dir > 0){
							mp_step.x = x;
							mp_step.y = y - TILE_SIZE * move_dir;
							dir = 90;
							vspeed = -move_speed;
							pos++;
						}
						else{
							mp_step.x = x;
							mp_step.y = y + TILE_SIZE * move_dir;
							dir = 270;
							vspeed = move_speed;
							pos--;
						}
						break;
					
					case(3):
						if (move_dir > 0){
							mp_step.x = x - TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 180;
							hspeed = -move_speed;
							pos++;
						}
						else{
							mp_step.x = x;
							mp_step.y = y + TILE_SIZE * move_dir;
							dir = 270;
							vspeed = move_speed;
							pos--;
						}
						break;
					
					case(4):
						if (move_dir > 0){
							mp_step.x = x - TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 180;
							hspeed = -move_speed;
							pos++;
						}
						else{
							mp_step.x = x + TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 0;
							hspeed = move_speed;
							pos--;
						}
						break;
					
					case(5):
						if (move_dir > 0){
							mp_step.x = x;
							mp_step.y = y + TILE_SIZE * move_dir;
							dir = 270;
							vspeed = move_speed;
							pos++;
						}
						else{
							mp_step.x = x + TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 0;
							hspeed = move_speed;
							pos--;
						}
						break;
					
					case(6):
						if (move_dir > 0){
							mp_step.x = x;
							mp_step.y = y + TILE_SIZE * move_dir;
							dir = 270;
							vspeed = move_speed;
							pos++;
						}
						else{
							mp_step.x = x;
							mp_step.y = y - TILE_SIZE * move_dir;
							dir = 90;
							vspeed = -move_speed;
							pos--;
						}
						break;
					
					case(7):
						if (move_dir > 0){
							mp_step.x = x + TILE_SIZE * move_dir;
							mp_step.y = y;
							dir = 0;
							hspeed = move_speed;
							pos++;
						}
						else{
							mp_step.x = x;
							mp_step.y = y - TILE_SIZE * move_dir;
							dir = 90;
							vspeed = -move_speed;
							pos--;
						}
						break;
				}
			
				if (pos > 7)
					pos = 0;
			
				if (pos < 0)
					pos = 7;
			
				with (sight_b){
					sight = false;
				}

				with (sight_c){
					sight = false;
				}
	
				cool = true;
			}
			else if(!obj_music.move){
				cool = false;
			}
			#endregion
		
			#region Change State
			if (instance_exists(obj_distraction)){
				dis_to_object = distance_to_object(obj_distraction);
				dir_to_object = point_direction(x, y, obj_distraction.x, obj_distraction.y);
				x_dis = abs(lengthdir_x(dis_to_object, dir_to_object));
				y_dis = abs(lengthdir_x(dis_to_object, dir_to_object));
	
				if (dis_to_object <= MAX_DISTANCE){
					state = enemy_state.suspicious;
					Delete_Array()
					return_object = instance_create_layer(x,y,"Management", obj_return);
					obj_awareness.awareness ++;
					audio_play_sound(guard_notices, 10, false);
					instance_create_layer(x, y - TILE_SIZE, "Instances_1", obj_suprise);
				}
			}
			#endregion
		
			break;
	
		case (enemy_state.suspicious):
			#region suspicious
			if (instance_exists(obj_distraction)){
				dis_to_object = distance_to_object(obj_distraction);
				dir_to_object = point_direction(x+TILE_SIZE/2, y-TILE_SIZE/2, obj_distraction.x+TILE_SIZE/2, obj_distraction.y-TILE_SIZE/2);
				x_dis = abs(lengthdir_x(dis_to_object, dir_to_object));
				y_dis = abs(lengthdir_x(dis_to_object, dir_to_object));
			
				if (dis_to_object <= MAX_DISTANCE){
					if (obj_music.move && !cool){
						with (sight_b){
							sight = false;
						}

						with (sight_c){
							sight = false;
						}
					
						if (dir_to_object <= 45 || dir_to_object >= 315){
							Predict(x + TILE_SIZE, y , x, y - TILE_SIZE, x, y + TILE_SIZE, x - TILE_SIZE, y, 0);
						}
					
						else if (dir_to_object > 45 && dir_to_object < 135){
							Predict(x, y - TILE_SIZE, x - TILE_SIZE, y, x + TILE_SIZE, y, x, y + TILE_SIZE, 90);
						}
					
						else if (dir_to_object >= 135 && dir_to_object <= 225){
							Predict(x - TILE_SIZE, y , x, y - TILE_SIZE, x, y + TILE_SIZE, x + TILE_SIZE, y, 0);
						}
					
						else if (dir_to_object > 225 && dir_to_object < 315){
							Predict(x, y + TILE_SIZE, x - TILE_SIZE, y, x + TILE_SIZE, y, x, y - TILE_SIZE, 90);
						}
						cool = true;
					}
					else if (!obj_music.move){
						cool = false;
					}
				}
				else{
					state = enemy_state.returning;
					obj_awareness.awareness --;
					Delete_Array();
				}
			}
			else{
				state = enemy_state.returning;
				obj_awareness.awareness --;
				Delete_Array();
			}
			#endregion
			break;
		
		case (enemy_state.returning):
			#region returning
			dir_to_object = point_direction(x+TILE_SIZE/2, y-TILE_SIZE/2, return_object.x+TILE_SIZE/2, return_object.y-TILE_SIZE/2);
		
			if (obj_music.move && !cool){
				with (sight_b){
					sight = false;
				}

				with (sight_c){
					sight = false;
				}
			
				if (dir_to_object <= 45 || dir_to_object >= 315){
					Predict(x + TILE_SIZE, y , x, y - TILE_SIZE, x, y + TILE_SIZE, x - TILE_SIZE, y, 0);
				}
					
				else if (dir_to_object > 45 && dir_to_object < 135){
					Predict(x, y - TILE_SIZE, x - TILE_SIZE, y, x + TILE_SIZE, y, x, y + TILE_SIZE, 90);
				}
					
				else if (dir_to_object >= 135 && dir_to_object <= 225){
					Predict(x - TILE_SIZE, y , x, y - TILE_SIZE, x, y + TILE_SIZE, x + TILE_SIZE, y, 0);
				}
					
				else if (dir_to_object > 225 && dir_to_object < 315){
					Predict(x, y + TILE_SIZE, x - TILE_SIZE, y, x + TILE_SIZE, y, x, y - TILE_SIZE, 90);
				}
				cool = true;
			}
			else if (!obj_music.move){
					cool = false;
				}
		
			if place_meeting(x,y, return_object){
				instance_destroy(return_object);
				state = enemy_state.patrol;
				Delete_Array();
			}
		
			if (instance_exists(obj_distraction)){
				dis_to_object = distance_to_object(obj_distraction);
				dir_to_object = point_direction(x, y, obj_distraction.x, obj_distraction.y);
				x_dis = abs(lengthdir_x(dis_to_object, dir_to_object));
				y_dis = abs(lengthdir_x(dis_to_object, dir_to_object));
	
				if (dis_to_object <= MAX_DISTANCE){
					state = enemy_state.suspicious;
					obj_awareness.awareness ++;
					Delete_Array();
					audio_play_sound(guard_notices, 10, false);
					instance_create_layer(x, y - TILE_SIZE, "Instances_1", obj_suprise);
				}
			}
		#endregion
	}

	#region speed check
	if (speed != 0){
		if (hspeed < 0 && x <= mp_step.x){
				hspeed = 0
				x = mp_step.x
				dir = 180;
		}
	
		if (hspeed > 0 && x >= mp_step.x){
				hspeed = 0
				x = mp_step.x
				dir = 0;
		}
	
		if (vspeed < 0 && y <= mp_step.y){
				vspeed = 0
				y = mp_step.y
				dir = 90;
		}
	
		if (vspeed > 0 && y >= mp_step.y){
				vspeed = 0
				y = mp_step.y
				dir = 270;
		}
	
	}
	#endregion

	Flashlight();
}
else{
	speed = 0
}