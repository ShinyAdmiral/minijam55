{
    "id": "507aae6c-cbfa-4bfa-ac0e-b536724c0aad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guard_Circle",
    "eventList": [
        {
            "id": "b4ce2dd4-e5d8-44e0-ba71-91d2c9316f68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "507aae6c-cbfa-4bfa-ac0e-b536724c0aad"
        },
        {
            "id": "73ba7c27-2f9d-473d-8a6a-39e72cf789a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "507aae6c-cbfa-4bfa-ac0e-b536724c0aad"
        },
        {
            "id": "35f9c629-d3c5-4471-bc08-bb58ccff96c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "507aae6c-cbfa-4bfa-ac0e-b536724c0aad"
        },
        {
            "id": "92ae610a-3d4d-414f-8093-6120b7771fad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "507aae6c-cbfa-4bfa-ac0e-b536724c0aad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "16728db2-6741-4e9c-b4d0-fa7ea70c8f2f",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "facing",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "5659f7e7-1a90-40f6-a90e-06ec46b6bb44",
    "visible": true
}