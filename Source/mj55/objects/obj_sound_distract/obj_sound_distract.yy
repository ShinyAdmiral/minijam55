{
    "id": "9dcaafc3-d74b-4da9-88ad-23b6a299b419",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sound_distract",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1e8015ca-779b-4732-a824-668beafa6c80",
    "visible": true
}