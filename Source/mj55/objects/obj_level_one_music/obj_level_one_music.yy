{
    "id": "e9d80849-25ee-4333-8077-86a3780323bb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_level_one_music",
    "eventList": [
        {
            "id": "4a507743-b3d4-4005-810b-f8ae37223e1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9d80849-25ee-4333-8077-86a3780323bb"
        },
        {
            "id": "9c5de5a9-ec0b-41f2-8e53-df7762cacd30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e9d80849-25ee-4333-8077-86a3780323bb"
        },
        {
            "id": "61bd6960-33d5-4d13-9225-0f1c31a08433",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "e9d80849-25ee-4333-8077-86a3780323bb"
        },
        {
            "id": "f81bab5e-722e-48cf-b25c-34359acba126",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "e9d80849-25ee-4333-8077-86a3780323bb"
        },
        {
            "id": "582725fd-585f-4fd4-aa89-120405edb2ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9d80849-25ee-4333-8077-86a3780323bb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b1e8beaf-3508-4bb8-b76c-9a4574b26a72",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}