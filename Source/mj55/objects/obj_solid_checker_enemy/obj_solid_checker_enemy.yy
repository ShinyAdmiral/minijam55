{
    "id": "6866fd62-5177-4f55-a37a-3456bec29614",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid_checker_enemy",
    "eventList": [
        {
            "id": "c43779ea-414c-4bcb-adc5-b5dd41cf7658",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6866fd62-5177-4f55-a37a-3456bec29614"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "964a0922-964c-4bab-abd6-e44e104eb88a",
    "visible": true
}