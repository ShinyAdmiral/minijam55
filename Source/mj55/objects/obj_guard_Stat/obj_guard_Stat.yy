{
    "id": "1fead751-5ff6-40b3-b759-8ac082e8773f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guard_Stat",
    "eventList": [
        {
            "id": "9521d13b-c9ad-4c4b-b487-0b3e4c120ed3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1fead751-5ff6-40b3-b759-8ac082e8773f"
        },
        {
            "id": "f79c917a-23f8-4e2f-8b9b-026fddb17122",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1fead751-5ff6-40b3-b759-8ac082e8773f"
        },
        {
            "id": "751df05d-ea42-487d-a8b7-43769f5c35a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1fead751-5ff6-40b3-b759-8ac082e8773f"
        },
        {
            "id": "d3388a41-9a78-42f4-a746-c847b3402a1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1fead751-5ff6-40b3-b759-8ac082e8773f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "6f90f9c6-124c-4bb4-963d-ff20119ec41e",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "facing",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "f0c0e1c8-9bcd-4905-bc32-f53422d878c2",
    "visible": true
}