/// @description Insert description here
//progress
if (obj_player.operate){
	draw_set_font(font_painting);
	draw_set_color(c_white);
	str = string (treasures) + " / " + string (max_treasurs);
	x_display = x_pos_treasures - (string_length(str) * font_get_size(font_painting)/treasure_size_buffer);
	draw_text(x_display, y_pos_treasures, str);
	
	draw_sprite(spr_ui_paintingicon, 0, x_display + 30,y_pos_treasures-7)
	
	str = string(coins);
	x_display = x_pos_treasures - (string_length(str) * font_get_size(font_painting)/treasure_size_buffer)
	draw_text(x_display, y_pos_treasures + y_buffer, str);
	draw_sprite(spr_distaction1, 0, x_display + 15,y_pos_treasures + y_buffer - 5)

	//timer
	decisecs = string(time mod 10);
	if ((time div 10) mod 60) >= 10{
		seconds = string((time div 10) mod 60);
	}
	else{
		seconds = "0" + string((time div 10) mod 60);
	}

	minutes = string((time div 600) mod 60);

	str = minutes + ":" + seconds + ":" + decisecs;
	x_display = 60 - (string_length(str) * font_get_size(font_painting)/treasure_size_buffer)

	draw_text(x_display, y_pos_treasures, str);
	draw_sprite(spr_ui_stopwatch, 0, 5,y_pos_treasures- 7)
}