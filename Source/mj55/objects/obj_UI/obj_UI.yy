{
    "id": "1e5397c8-bf96-45fe-8ff9-8a71e1b018cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_UI",
    "eventList": [
        {
            "id": "5542a7e8-92b1-4037-9d18-5658d106cefa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1e5397c8-bf96-45fe-8ff9-8a71e1b018cb"
        },
        {
            "id": "e9467943-665e-49ac-9d35-ca9a7a695db7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1e5397c8-bf96-45fe-8ff9-8a71e1b018cb"
        },
        {
            "id": "3554e7a9-9d80-4684-82ab-1358c9705385",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1e5397c8-bf96-45fe-8ff9-8a71e1b018cb"
        },
        {
            "id": "c9b4ed93-01df-46e2-8428-73341bed14c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1e5397c8-bf96-45fe-8ff9-8a71e1b018cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}