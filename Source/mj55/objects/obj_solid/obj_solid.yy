{
    "id": "b11a3e79-f1de-4c7e-8402-a89a66284f76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_solid",
    "eventList": [
        {
            "id": "ccf82d06-5417-4f4b-8ed5-8f024f20d71d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b11a3e79-f1de-4c7e-8402-a89a66284f76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "659c1ed8-6be9-4c9b-9a9a-5d0762ca1f13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d387c3f9-9a4c-49b2-9739-6ee3491cf479",
    "visible": true
}