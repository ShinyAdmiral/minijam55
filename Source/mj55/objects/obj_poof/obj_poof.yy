{
    "id": "61c6cd91-0fe1-4d9f-b890-77cefcf4b478",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_poof",
    "eventList": [
        {
            "id": "9260b07b-1265-4f82-9c63-66ec936ad57e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "61c6cd91-0fe1-4d9f-b890-77cefcf4b478"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d2802cb-d379-4260-938b-38e70efb70d7",
    "visible": true
}