{
    "id": "6964d05e-3134-44e2-b2f4-66b055ba84ad",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Guard_light",
    "eventList": [
        {
            "id": "29fe3e2b-740b-4c20-a31e-8dc5ae67cb62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6964d05e-3134-44e2-b2f4-66b055ba84ad"
        },
        {
            "id": "1e63fd08-be25-401e-9e55-372cb1bb5062",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6964d05e-3134-44e2-b2f4-66b055ba84ad"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "a30522fd-ba5f-4281-a6f5-ca6ed50f63d7",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "12977b57-3c14-4684-a860-15eef892e37e",
            "value": "\"Spot Light\""
        },
        {
            "id": "3d8289a9-d051-4840-9718-2ff7852ff5b9",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "acc21797-5541-499d-a7a7-ca1316572c37",
            "value": "$FF00FFFF"
        },
        {
            "id": "2674b6ca-f9b9-46b4-8973-aac4d6c655f5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "5686c0db-78e4-4fd7-89e5-0ed522384dd8",
            "value": "20"
        },
        {
            "id": "164fd1b6-c3b4-40e2-923c-463a50e05509",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "1a4e0384-2c2a-48c1-86ff-1579eb84c5fa",
            "value": "150"
        },
        {
            "id": "52e74385-edd0-42c1-8164-2b4e20a65b19",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "a89637b7-5f88-4100-a038-c4d3e5c67480",
            "value": "3.9"
        },
        {
            "id": "b937f0a9-d7f0-46fd-9992-7f0afee7c8c9",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "b70554c1-2210-49d5-84db-fb0c2df6cbc1",
            "value": "0"
        }
    ],
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}