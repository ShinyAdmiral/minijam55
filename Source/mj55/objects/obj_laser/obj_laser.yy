{
    "id": "ebb27f8b-03da-4d38-8e52-846cd2f1ff76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_laser",
    "eventList": [
        {
            "id": "6cd72062-c4ec-476f-9e11-d3390db97773",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ebb27f8b-03da-4d38-8e52-846cd2f1ff76"
        },
        {
            "id": "a08ad7e6-671e-4d20-8ce8-653d4f9366e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ebb27f8b-03da-4d38-8e52-846cd2f1ff76"
        },
        {
            "id": "66057850-81af-4340-b6a7-422b1ca0bfb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "ebb27f8b-03da-4d38-8e52-846cd2f1ff76"
        },
        {
            "id": "95faebca-b660-4d96-97a4-3b8a1d86be72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebb27f8b-03da-4d38-8e52-846cd2f1ff76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "6b1939cf-1f20-4429-be1e-5a4f9dca2cf7",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "acc21797-5541-499d-a7a7-ca1316572c37",
            "value": "$FF0000FF"
        },
        {
            "id": "1ea290fb-8c1e-495b-8a3f-a986817dd007",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "1a4e0384-2c2a-48c1-86ff-1579eb84c5fa",
            "value": "50"
        },
        {
            "id": "a22a35e7-6aff-48d7-ae12-256e6c8ab209",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "a89637b7-5f88-4100-a038-c4d3e5c67480",
            "value": "14.7"
        },
        {
            "id": "6fd94e2f-ef28-4e1a-88c5-f3feb91ef249",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "1cb40d30-25a7-4fac-beac-2258d74cb99b",
            "value": "25"
        },
        {
            "id": "e79731d1-3cce-4cad-a0de-3d722c361462",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
            "propertyId": "12977b57-3c14-4684-a860-15eef892e37e",
            "value": "\"Point Light\""
        }
    ],
    "parentObjectId": "84334f4e-b6d2-4ffe-89cf-f8641f75877e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "c2b89648-9f0a-48d0-b450-27bf6745a850",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "variable_name",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "cbc0f9e3-2aac-4500-9569-3cf610c81a27",
    "visible": true
}