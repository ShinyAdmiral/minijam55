/// @description Insert description here
// Inherit the parent event
event_inherited();

light[|eLight.X] = x + TILE_SIZE/2;
light[|eLight.Y] = y + TILE_SIZE/2;

sprite_index = spr_beam;

alarm[0] = 1;