/// @description Insert description here
operate = false;

if (light){
	instance_create_layer(x + TILE_SIZE,y,"Light_Renderer", obj_Player_spot);
	instance_create_layer(x,y + TILE_SIZE,"Light_Renderer", obj_Player_spot);
	instance_create_layer(x+ TILE_SIZE,y+ TILE_SIZE,"Light_Renderer", obj_Player_spot);
	instance_create_layer(x,y,"Light_Renderer", obj_Player_spot);
	instance_create_layer(0,0,"Instances_1", obj_whitescreen);
	with(obj_guard){
		instance_create_layer(x, y - TILE_SIZE, "Treasures", obj_suprise1);
	}
	audio_play_sound(sfx_Spotlight,10,false);
	image_speed = 0;
	speed = 0;
	light = false;
}