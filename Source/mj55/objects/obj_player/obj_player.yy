{
    "id": "a0500300-af83-489e-a58b-9c1e1152179c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "7f33c055-c608-4265-8c53-b60fd9d81c8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a0500300-af83-489e-a58b-9c1e1152179c"
        },
        {
            "id": "57d87c05-4df1-4c50-a010-437460453241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0500300-af83-489e-a58b-9c1e1152179c"
        },
        {
            "id": "b1a648df-b72b-4d19-96bb-d779cdf9726c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d87109a7-3cd1-4547-ba55-c3936f51c157",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a0500300-af83-489e-a58b-9c1e1152179c"
        },
        {
            "id": "5d4db1a0-81a3-4426-a333-788852e19e32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d73ed53a-fc0c-447b-ae16-709af2b5e440",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a0500300-af83-489e-a58b-9c1e1152179c"
        },
        {
            "id": "7bbce3bb-6d72-4cfe-a016-d54bb45846db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ebb27f8b-03da-4d38-8e52-846cd2f1ff76",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a0500300-af83-489e-a58b-9c1e1152179c"
        },
        {
            "id": "557a4d42-7952-413c-847d-499fba336e4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a0500300-af83-489e-a58b-9c1e1152179c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "659c1ed8-6be9-4c9b-9a9a-5d0762ca1f13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26520a86-5512-4b66-bb6f-5d7929a1b4db",
    "visible": true
}