/// @description Insert description here
Depth_Update(0);

if (operate){
	polygon = polygon_from_instance(id);

	//gather input
	move_up = keyboard_check_pressed(ord("W"));
	move_down = keyboard_check_pressed(ord("S"));
	move_left = keyboard_check_pressed(ord("A"));
	move_right = keyboard_check_pressed(ord("D"));
	distract = keyboard_check_pressed(vk_space);

	if (move_up){
		sprite_index = spr_Backwards_Idle;
		dir = 90;
	}
	else if(move_down){
		sprite_index = spr_Forwards_Idle;
		dir = 270;
	}
	else if(move_left){
		sprite_index = spr_Left_Idle;
		dir = 180;
	}
	else if(move_right){
		sprite_index = spr_Right_Idle;
		dir = 0;
	}

	if (obj_music.player_move){
	
		//reset and set movement values
		horizontal_check = 0;
		vertical_check = 0;

		if (move_up || move_down){
			vertical_check = (move_down - move_up) * TILE_SIZE;
			vertical_movement = (move_down - move_up) * move_speed;
			noisey = false;
			obj_music.player_move = false;
		}

		else if (move_left || move_right){
			horizontal_check = (move_right - move_left) * TILE_SIZE;
			horizontal_movement = (move_right - move_left) * move_speed;
			noisey = false;
			obj_music.player_move = false;
		}
	
		else if (distract){
			if (obj_UI.coins > 0){
				if (instance_exists(obj_distraction)){
					if (instance_exists(obj_coin_distract))
						instance_create_layer(obj_distraction.x, obj_distraction.y, "Instances", obj_coin);
					instance_destroy(obj_distraction);
				}
				switch (dir){
					case(0):
						if (!place_meeting(x+TILE_SIZE, y, obj_solid)){
							instance_create_layer(x + TILE_SIZE, y, "Distraction", obj_coin_distract);
							sprite_index = spr_throw_r;
						}
						break;
					case(90):
						if (!place_meeting(x, y - TILE_SIZE, obj_solid)){
							instance_create_layer(x, y - TILE_SIZE, "Distraction", obj_coin_distract);
							sprite_index = spr_throw_b;
						}
						break;
					case(180):
						if (!place_meeting(x - TILE_SIZE, y, obj_solid)){
							instance_create_layer(x - TILE_SIZE, y, "Distraction", obj_coin_distract);
							sprite_index = spr_throw_l;
						}
						break;
					case(270):
						if (!place_meeting(x, y + TILE_SIZE, obj_solid)){
							instance_create_layer(x, y + TILE_SIZE, "Distraction", obj_coin_distract);
							sprite_index = spr_throw_f;
						}
						break;
				}
				ran = irandom_range(.8, 1.2);
				audio_sound_pitch(coin_drop_sfx, ran);
				audio_play_sound(coin_drop_sfx, 10, false);
				obj_UI.coins--;
			}
		}
	
		obj_solid_checker.x = x + horizontal_check;
		obj_solid_checker.y = y + vertical_check;

		with (obj_solid_checker){
			if (!place_meeting(x,y,obj_solid)){
				//obj_player.x += obj_player.horizontal_check;
				//obj_player.y += obj_player.vertical_check;
				obj_player.hspeed = obj_player.horizontal_movement;
				obj_player.vspeed = obj_player.vertical_movement;
				if (place_meeting(x, y, obj_noisy_floor)){
					instance_create_layer(x,y,"Management", obj_sound_distract);
					if(obj_player.noisey){
						audio_play_sound(coin_drop_sfx,10,false);
						obj_player.noisey = true;
					}
				}
			}
	
			inst = instance_place(x, y, obj_painting);
	
			if (inst != noone){
				obj_UI.treasures ++;
				instance_destroy(inst);
			}
		}
	}

	else if (move_up || move_down || move_left || move_right || distract){
		obj_music.player_move = false;
		obj_music.skip = true;
	}

	if (speed != 0){
		if (hspeed < 0 && x <= obj_solid_checker.x){
				hspeed = 0
				x = obj_solid_checker.x
		}
	
		if (hspeed > 0 && x >= obj_solid_checker.x){
				hspeed = 0
				x = obj_solid_checker.x
		}
	
		if (vspeed < 0 && y <= obj_solid_checker.y){
				vspeed = 0
				y = obj_solid_checker.y
		}
	
		if (vspeed > 0 && y >= obj_solid_checker.y){
				vspeed = 0
		}
	
	}
}