{
    "id": "77af10a5-4b61-4003-a874-9e2d6e54e3fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_guard_ver",
    "eventList": [
        {
            "id": "55910219-06d3-4379-9986-767700580778",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "77af10a5-4b61-4003-a874-9e2d6e54e3fb"
        },
        {
            "id": "2e60a0b4-8f18-4fe3-a52d-e96b908a89e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "77af10a5-4b61-4003-a874-9e2d6e54e3fb"
        },
        {
            "id": "70c3ed95-5a3b-4713-899c-3bf52ccd47bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "69a03dcc-76a2-41cb-bc10-f3d2325f6908",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "77af10a5-4b61-4003-a874-9e2d6e54e3fb"
        },
        {
            "id": "6c36cf91-062d-4316-b638-92ad702b1f4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "77af10a5-4b61-4003-a874-9e2d6e54e3fb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "8a0e568e-dc59-4230-80c8-0318c5f4d613",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "70d566f0-9825-418e-9898-87d55cfcd11b",
    "visible": true
}